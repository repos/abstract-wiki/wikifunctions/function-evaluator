# Executor Rust Prototype

## Usage

```
cargo build -r
./target/release/executor-example ./python3/rustpython_uncompiled.wasm <executor request file>"
```

You should see the executor's bespoke message-passing output, e.g. 

```
./target/release/executor-example ./python3/rustpython_uncompiled.wasm ../test_data/python3_add.json
Stdout contained:
start internal log <<<
calling execute in executor..., original time: 2024-11-05T01:05:41.860587
>>> end internal log
start internal log <<<
...finished calling execute in executor, original time: 2024-11-05T01:05:41.861037
>>> end internal log
start result <<<
[{"Z1K1": "Z6", "Z6K1": "13"}, null]
>>> end result
```

## TODO

- concurrency example: run two executors at once
- custom Drop logic for Executor to ensure that internal Future concludes one way or another, preserving lifetime guarantees
    - probably need an `AbortFuture` for this, since `run_function_call` could enter an infinite loop
        - or a timeout if that's possible :thinking:
- X DONE X Blubberization
    - pull executor source code and rebuild .wasm binary
- X DONE X custom `ReadPipe` and `WritePipe` for async communication between host and WASI

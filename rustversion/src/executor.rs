use crossbeam_channel::{Receiver, Sender, SendError};
use futures::future::BoxFuture;
use std::{
    io::{Read, Write},
    sync::{Arc, RwLock}
};
use wasi_common::{
    pipe::{ReadPipe, WritePipe},
    tokio::WasiCtxBuilder,
    WasiCtx
};
use wasmtime::{Engine, Linker, Module, Result, Store, Config};

use crate::executor_classes::{ExecutorErrors, ExecutorWasiBuilder};
use crate::state_machine::{StateMachine};

struct StateMachineWriter {
    state_machine: Arc<RwLock<StateMachine>>,
    buffer: Vec<u8>
}

impl StateMachineWriter {

    fn new(state_machine: Arc<RwLock<StateMachine>>) -> Self {
        Self{
            state_machine,
            buffer: Vec::<u8>::new()
        }
    }

}

impl Write for StateMachineWriter {

    fn write(&mut self, buf: &[u8]) -> std::result::Result<usize, std::io::Error> {
        let mut bytes_written: usize = 0;
        for character in buf {
            if *character == 10 {
                // TODO (T380808): Possibly don't use flush() here?
                // Also, handle the error from this Result.
                let _ = self.flush();
            } else {
                self.buffer.push(*character);
            }
            bytes_written += 1;
        }
        Ok(bytes_written)
    }

    fn flush(&mut self) -> std::result::Result<(), std::io::Error> {
        // Consume the internal buffer (should be empty after this operation).
        let mut new_buffer = Vec::new();
        std::mem::swap(&mut self.buffer, &mut new_buffer);
        let message_result = String::from_utf8(new_buffer);
        assert!(message_result.is_ok());
        let message = message_result.unwrap();
        let try_locked_state_machine = self.state_machine.write();
        assert!(try_locked_state_machine.is_ok());
        let mut locked_state_machine = try_locked_state_machine.unwrap();
        locked_state_machine.process_message( &message );
        Ok(())
    }
}

struct ChanneledReader {
    has_received: bool,
    receiver: Receiver<String>
}

impl ChanneledReader {

    fn new(receiver: Receiver<String>) -> Self {
        Self{has_received: false, receiver}
    }

}

impl Read for ChanneledReader {

    fn read(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        let mut index: usize = 0;
        if !self.has_received {
            let string_data = self.receiver.recv().unwrap();
            for byte in string_data.as_bytes() {
                buf[index] = *byte;
                index += 1;
            }
            self.has_received = true;
        }
        Ok(index)
    }

}

// TODO (T380808): Find a more idiomatic pattern for this. The idea here is to be able
// to surface a number of error types with ?. Here, we're just wrapping them
// in an enum and losing the original error.
#[derive(Debug)]
pub enum AcceptedErrors {
    CrossbeamError(()),
    ExecutorError(()),
    StdError(()),
    StringArrayError(()),
    WasiError(()),
    WasmtimeError(()),
}

impl From<SendError<String>> for AcceptedErrors {
    fn from(_error: SendError<String>) -> AcceptedErrors {
        AcceptedErrors::CrossbeamError(())
    }
}

impl From<wasi_common::StringArrayError> for AcceptedErrors {
    fn from(_error: wasi_common::StringArrayError) -> AcceptedErrors {
        AcceptedErrors::StringArrayError(())
    }
}

impl From<wasi_common::Error> for AcceptedErrors {
    fn from(_error: wasi_common::Error) -> AcceptedErrors {
        AcceptedErrors::WasiError(())
    }
}

impl From<wasmtime::Error> for AcceptedErrors {
    fn from(_error: wasmtime::Error) -> AcceptedErrors {
        AcceptedErrors::WasmtimeError(())
    }
}

impl From<std::io::Error> for AcceptedErrors {
    fn from(_error: std::io::Error) -> AcceptedErrors {
        AcceptedErrors::StdError(())
    }
}

impl From<ExecutorErrors> for AcceptedErrors {
    fn from(_error: ExecutorErrors) -> AcceptedErrors {
        AcceptedErrors::ExecutorError(())
    }
}

pub struct Executor<'a> {
    execution_result_future: Option<BoxFuture<'a, Result<(), AcceptedErrors>>>,
    sender: Sender<String>,
    state_machine_locked: Arc<RwLock<StateMachine>>
}

async fn run_function_call<'a, 'b, T: ExecutorWasiBuilder + 'a>(wasm_file: &'b str, receiver: Receiver<String>, state_machine: Arc<RwLock<StateMachine>>) -> Result<(), AcceptedErrors> {
    println!("Calling run_function_call; creating WASI stuff ...");
    let mut config = Config::new();
    config.async_support(true);
    let engine = Engine::new(&config).unwrap();
    let mut linker = Linker::<WasiCtx>::new(&engine);
    let linkage_result = wasi_common::tokio::add_to_linker(&mut linker, |s| s);
    assert!(linkage_result.is_ok());
    let module = Module::from_file(&engine, wasm_file).unwrap();

    // Prepare stdin.
    let stdin_reader = ChanneledReader::new(receiver);
    let stdin_pipe = ReadPipe::new(stdin_reader);

    let stdout_callback = StateMachineWriter::new(state_machine);
    let stdout_pipe = WritePipe::new(stdout_callback);

    println!("Building WASI context ...");
    let mut wasi_builder = WasiCtxBuilder::new();
    wasi_builder
        .stdin(Box::new(stdin_pipe))
        .stdout(Box::new(stdout_pipe));
    T::build_for_executor(&mut wasi_builder, wasm_file)?;
    let wasi = wasi_builder.build();

    println!("Calling WASM binary's _start function ...");
    let mut store = Store::new(
        &engine,
        wasi
    );
    linker.allow_unknown_exports(true);
    linker.define_unknown_imports_as_default_values(&module)?; 
    let linking = linker.instantiate_async(&mut store, &module).await?;
    let start = linking.get_typed_func::<(), ()>(&mut store, "_start")?;
    start.call_async(&mut store, ()).await?;
    Ok(())
}

impl<'a> Executor<'a> {

    pub fn new<'b, T: ExecutorWasiBuilder + 'a> (wasm_file: &'b str) -> Self where 'b: 'a {
        let (sender, receiver) = crossbeam_channel::unbounded();

        // Prepare StateMachine to handle messages passed over stdout.
        let state_machine = StateMachine::new();

        // TODO (T380809): It is possible that a simple Mutex can be used here,
        // rather than an RwLock.
        // More likely, the state machine
        // need not be locked--it could be moved after the WasiCtx goes out
        // of scope--and a Channel is probably the better solution.
        // In any case, this is okay for now.
        let state_machine_locked = Arc::new(RwLock::new(state_machine));
        let state_machine_clone = state_machine_locked.clone();
        let result_future = run_function_call::<T>(wasm_file, receiver, state_machine_clone );
        let execution_result_future: Option<BoxFuture<'a, Result<(), AcceptedErrors>>> = Some( Box::pin( result_future ) );
        Self{
            execution_result_future,
            sender,
            state_machine_locked
        }
    }

    pub async fn execute(&mut self, json_input: String) -> Result<String, AcceptedErrors> {
        self.sender.send(json_input)?;
        let future_or_nothing = std::mem::take( &mut self.execution_result_future );
        let execution_result = match future_or_nothing {
            Some( the_future) => {
                // TODO (T380808): Do something with the result of the_future.await;
                // in particular, handle the error case.
                the_future.await?;
                let try_locked_state_machine = self.state_machine_locked.write();
                assert!(try_locked_state_machine.is_ok());
                let mut locked_state_machine = try_locked_state_machine.unwrap();
                let maybe_execution_result = locked_state_machine.result_string_moved();
                match maybe_execution_result {
                    Some(result_string) => result_string,
                    None => {
                        println!("Puzzlingly, could not get the result.");
                        String::from("not hay")
                    }
                }
            },
            None => {
                println!("Already executed!");
                String::from("not hay")
            }
        };
        Ok(execution_result)
    }

}

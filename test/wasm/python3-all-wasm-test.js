'use strict';

const fetch = require( '../../lib/fetch.js' );
const Server = require( '../utils/server.js' );
const { evaluatorIntegrationTest, executorClassTest } = require( '../utils/integrationTest.js' );
const { pythonWasmTestData, pythonWasmSkipTestData, pythonWasmLimitsTestData } = require( '../utils/data' );
const { Evaluator } = require( '../../src/Evaluator.js' );

describe( 'evaluate-wasm', function () {

	this.timeout( 20000 );

	let uri = null;
	const server = new Server();
	const fetchResult = async function ( requestBody ) {
		return await fetch( uri, requestBody );
	};
	const testRunner = function ( testName, callBack ) {
		it( testName, callBack );
	};
	const testRunnerSkip = function ( testName, callBack ) {
		it.skip( testName, callBack );
	};

	before( async () => {
		await server.start();
		uri = `${ server.config.uri }wikifunctions.org/v1/evaluate/`;
	} );

	after( async () => {
		server.stop();
		// This is necessary to ensure that all spawned processes are killed;
		// without this, CI runners will wait forever because the image still
		// has active processes. If only JS had destructors ...
		await Evaluator.drainTheSwamps();
	} );

	for ( const testData of pythonWasmTestData ) {
		evaluatorIntegrationTest(
			fetchResult,
			testRunner,
			testData.name,
			testData.input,
			testData.expectedOutput,
			testData.expectedErrorType,
			testData.expectedErrorKeyPhrase,
			testData.expectedExtraMetadata,
			testData.expectedMissingMetadata,
			testData.useBinaryFormatVersion
		);
	}

	for ( const testData of pythonWasmSkipTestData ) {
		evaluatorIntegrationTest(
			fetchResult,
			testRunnerSkip,
			testData.name,
			testData.input,
			testData.expectedOutput,
			testData.expectedErrorType,
			testData.expectedErrorKeyPhrase,
			testData.expectedExtraMetadata,
			testData.expectedMissingMetadata,
			testData.useBinaryFormatVersion
		);
	}

	for ( const testData of pythonWasmLimitsTestData ) {
		executorClassTest(
			testData.environment,
			testRunner,
			testData.name,
			testData.input,
			testData.expectedOutput,
			testData.expectedErrorType,
			testData.expectedErrorKeyPhrase,
			testData.expectedExtraMetadata,
			testData.expectedMissingMetadata
		);
	}

} );

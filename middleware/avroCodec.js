'use strict';

const { getWrappedZObjectFromVersionedBinary } = require( '../executors/javascript-wasmedge/function-schemata/javascript/src/serialize.js' );
const { makeMappedResultEnvelope, wrapInZ6 } = require( '../executors/javascript-wasmedge/function-schemata/javascript/src/utils.js' );
const { error, makeErrorInNormalForm } = require( '../executors/javascript-wasmedge/function-schemata/javascript/src/error.js' );
const ErrorFormatter = require( '../executors/javascript-wasmedge/function-schemata/javascript/src/errorFormatter' );

function transformRequestBodyMiddleware( req, res, next ) {
	let ZObject;
	try {
		ZObject = getWrappedZObjectFromVersionedBinary( req.body );
	} catch ( err ) {
		const message = `Error thrown by getWrappedZObjectFromVersionedBinary(): ${ err }.`;
		const wrappedError = ErrorFormatter.createZErrorInstance(
			error.unknown_error, { errorInformation: message } );
		res.json(
			makeMappedResultEnvelope(
				null,
				makeErrorInNormalForm( error.invalid_evaluation_request, [ wrappedError ] )
			)
		);
		return;
	}

	let reentrant = false;

	// Also support reentrant mode.
	if ( ZObject.reentrant !== undefined ) {
		reentrant = ZObject.reentrant;
	}

	if ( ZObject.zobject !== undefined ) {
		ZObject = ZObject.zobject;
	}

	// Get the coding language.
	const codingLanguagePropertyName = 'codingLanguage';
	const codingLanguage = ZObject[ codingLanguagePropertyName ];

	// We use ! here because null, undefined, and empty string are all bad values
	// for a programming language (although there's almost certainly an esolang called
	// "").
	if ( !codingLanguage ) {
		res.json(
			makeMappedResultEnvelope(
				null,
				makeErrorInNormalForm(
					error.incomplete_evaluation_request,
					[ wrapInZ6( codingLanguagePropertyName ) ]
				)
			)
		);
		return;
	}

	// Fill out the rest of the function call request.
	const functionCallRequest = {
		codingLanguage: codingLanguage,
		functionArguments: {},
		reentrant: reentrant,
		requestId: ZObject.requestId || req.context.reqId
	};

	// Avro schema semver >= 0.1.0.
	const functionPropertyName = 'function';
	let theFunction = ZObject[ functionPropertyName ];
	if ( theFunction === undefined ) {

		// Avro schema semver < 0.1.0.
		const codeString = ZObject.codeString;
		const functionName = ZObject.functionName;
		if ( codeString === undefined || functionName === undefined ) {
			res.json(
				makeMappedResultEnvelope(
					null,
					makeErrorInNormalForm(
						error.incomplete_evaluation_request,
						[ wrapInZ6( functionPropertyName ) ]
					)
				)
			);
			return;
		} else {
			theFunction = { codeString: codeString, functionName: functionName };
		}
	}
	functionCallRequest.function = theFunction;

	// Serializer could be either undefined or null depending on Avro schema version.
	if ( ZObject.serializer ) {
		functionCallRequest.serializer = ZObject.serializer;
	} else {
		functionCallRequest.serializer = null;
	}

	const functionArguments = ZObject.functionArguments || {};
	for ( const key of Object.keys( functionArguments ) ) {
		const argument = functionArguments[ key ];

		// Avro schema semver < 0.1.0.
		if ( argument.object === undefined ) {
			functionArguments[ key ] = { object: argument, deserializer: null };
		}
	}
	functionCallRequest.functionArguments = functionArguments;

	const schemaVersion = ZObject.schemaVersion;
	if ( schemaVersion !== undefined ) {
		functionCallRequest.schemaVersion = schemaVersion;
	}

	req.body = functionCallRequest;
	next();
}

module.exports = { transformRequestBodyMiddleware };

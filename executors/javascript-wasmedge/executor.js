import { serialize, deserialize } from './serialization.js';
// eslint-disable-next-line no-unused-vars
import { ZObject, ZPair } from './ztypes.js';
import { Wikifunctions } from './wikifunctions.js';
import { isString } from './function-schemata/javascript/shared/es6UtilsModule.js';

// eslint-disable-next-line no-unused-vars
function runCodeObject( codeObject, argumentList, Wikifunctions ) {
	const resultCache = new Map();
	let callMe;
	const functionTemplate = `
        callMe = function() {
            ${ codeObject.codeString }
            const result = ${ codeObject.functionName }.apply( null, argumentList );
            resultCache.set( 'result', result );
        }
    `;
	// eslint-disable-next-line security/detect-eval-with-expression
	eval( functionTemplate ); // eslint-disable-line no-eval
	callMe();
	return resultCache.get( 'result' );
}

/**
 * Construct an error object having the given error type.  Only for use with error types
 * having a single property which can take a string value (e.g., Z500, Z565).
 *
 * @param {string} errorType ZID for an error type (e.g., 'Z500', 'Z565')
 * @param {string} message value for the Z5xxK1 property
 * @return {Object} a Z5/Error object
 */
function error( errorType, message ) {
	const k1Property = errorType + 'K1';
	const z5k2Value = {
		Z1K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
			Z7K1: { Z1K1: 'Z9', Z9K1: 'Z885' },
			Z885K1: { Z1K1: 'Z9', Z9K1: errorType }
		}
	};
	z5k2Value[ k1Property ] = { Z1K1: 'Z6', Z6K1: message };
	return {
		Z1K1: { Z1K1: 'Z9', Z9K1: 'Z5' },
		Z5K1: { Z1K1: 'Z9', Z9K1: errorType },
		Z5K2: z5k2Value
	};
}

async function executeInternal( functionCall, W ) {
	const theFunction = functionCall.function;

	// TODO (T282891): Handle input that fails to validate all at once instead of ad hoc.
	if ( theFunction === undefined ) {
		return {
			error: error( 'Z565', 'function' )
		};
	}
	if ( theFunction.functionName === undefined ) {
		return {
			error: error( 'Z565', 'functionName' )
		};
	}
	if ( theFunction.codeString === undefined ) {
		return {
			error: error( 'Z565', 'codeString' )
		};
	}

	// TODO (T289319): Consider whether to reduce all keys to local keys.
	const argumentNames = [];
	const boundValues = new Map();
	for ( const key of Object.keys( functionCall.functionArguments ) ) {
		argumentNames.push( key );
		const value = functionCall.functionArguments[ key ];
		const customDeserializer = value.deserializer;
		let deserialized;
		if ( customDeserializer ) {
			let deserializerCode, isList;
			if ( functionCall.schemaVersion === '0.1.1' ) {
				deserializerCode = customDeserializer.code;
				isList = customDeserializer.isList;
			} else {
				deserializerCode = customDeserializer;
				isList = false;
			}
			const deserializerFunction = ( argument ) => runCodeObject(
				deserializerCode, [ argument ], W
			);
			if ( isList ) {
				deserialized = deserialize( value.object, deserializerFunction );
			} else {
				deserialized = deserializerFunction( value.object );
			}
		} else {
			deserialized = deserialize( value.object );
		}
		boundValues.set( key, deserialized );
	}
	argumentNames.sort();
	const sortedArguments = [];
	for ( const key of argumentNames ) {
		const value = boundValues.get( key );
		sortedArguments.push( value );
	}

	const result = runCodeObject( theFunction, sortedArguments, W );

	const customSerializer = functionCall.serializer;

	let serialized;
	if ( customSerializer ) {
		let serializerCode, isList;
		if ( functionCall.schemaVersion === '0.1.1' ) {
			serializerCode = customSerializer.code;
			isList = customSerializer.isList;
		} else {
			serializerCode = customSerializer;
			isList = false;
		}
		const serializerFunction = ( argument ) => runCodeObject( serializerCode, [ argument ], W );
		if ( isList ) {
			serialized = serialize( result, serializerFunction );
		} else {
			serialized = serializerFunction( result );
		}
	} else {
		serialized = serialize( result );
	}

	return { result: serialized };
}

async function execute( functionCall, readFromStdin, writeToStdout ) {
	let resultOrError = {};
	const W = new Wikifunctions( readFromStdin, writeToStdout );
	try {
		resultOrError = await executeInternal( functionCall, W );
	} catch ( e ) {
		let errorString;
		if ( isString( e ) ) {
			errorString = e;
		} else {
			errorString = e.message;
		}
		resultOrError.error = error( 'Z500', errorString );
	}

	return [ resultOrError.result || null, resultOrError.error || null ];
}

async function runFunction( readFromStdin, writeToStdout ) {
	const writeInternalLog = ( logText ) => {
		const dateString = new Date().toISOString();
		// TODO (T370773): Provide logJSON as is, setting time: dateString.
		logText = logText + ', original time: ' + dateString;
		writeToStdout( 'start internal log <<<' );
		writeToStdout( logText );
		writeToStdout( '>>> end internal log' );
	};
	while ( true ) {
		const functionCallString = readFromStdin();
		if ( functionCallString.trim() === '' ) {
			continue;
		}
		const functionCall = JSON.parse( functionCallString );
		if ( functionCall ) {
			writeInternalLog( 'calling execute in executor...' );
			const result = await execute( functionCall, readFromStdin, writeToStdout );
			writeInternalLog( '...finished calling execute in executor' );
			writeToStdout( 'start result <<<' );
			writeToStdout( JSON.stringify( result ) );
			writeToStdout( '>>> end result' );
			break;
		}
	}
}

export { execute, runFunction };

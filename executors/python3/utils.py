from . import exceptions
from . import ztypes


# TODO (T282891): All _is_zwhatev functions should use function-schemata.
def _is_zreference(Z9):
    try:
        return Z9.get("Z1K1") == "Z9" and isinstance(Z9.get("Z9K1"), str)
    except AttributeError:
        return False


def _is_zfunction_call(Z7):
    try:
        return Z7.get("Z1K1", {}).get("Z9K1") == "Z7"
    except AttributeError:
        return False


def _is_zfunction(Z8):
    try:
        return Z8.get("Z1K1", {}).get("Z9K1") == "Z8"
    except AttributeError:
        return False


def _is_zMap(Z883):
    try:
        return Z883.get("Z1K1", {}).get("Z7K1", {}).get("Z9K1") == "Z883"
    except AttributeError:
        return False


def _is_zVoid(Z24):
    try:
        return Z24.get("Z9K1") == "Z24"
    except AttributeError:
        return False


def wrap_in_z9(reference):
    return {"Z1K1": "Z9", "Z9K1": reference}


def make_void():
    """Creates a Z24 (Void).

    Returns:
        ZObject corresponding to Z24
    """
    # TODO (T282891): Use function-schemata version.
    return {"Z1K1": "Z9", "Z9K1": "Z24"}


def frozendict(dictionary):
    """Creates an immutable representation of a dictionary."""
    if not isinstance(dictionary, dict):
        return dictionary
    result = []
    for key, value in dictionary.items():
        result.append((key, frozendict(value)))
    return frozenset(result)


def _get_head(zlist):
    return zlist.get("K1")


def _get_tail(zlist):
    return zlist.get("K2")


def is_empty_zlist(z_list):
    """Returns True iff input Typed List is empty."""
    return _get_head(z_list) is None


def convert_zlist_to_list(zlist):
    """Turns a Typed List into a Python list.

    Arguments:
        zlist: a Typed List

    Returns:
        a Python list containing all elements of the input Typed List
    """
    tail = zlist
    result = []
    while True:
        if is_empty_zlist(tail):
            break
        result.append(_get_head(tail))
        tail = _get_tail(tail)
    return result


def get_list_type(the_list):
    Z1K1s = set()
    for i, element in enumerate(the_list):
        if i == 0:
            first_Z1K1 = element["Z1K1"]
        # TODO (T293915): Use ZObjectKeyFactory or similar to create string representations.
        Z1K1s.add(frozendict(element["Z1K1"]))
    if len(Z1K1s) == 1:
        head_type = first_Z1K1
        # When element is a string, Z1K1 has a terminal value and not an object
        if isinstance(head_type, str):
            head_type = wrap_in_z9(head_type)
    else:
        head_type = wrap_in_z9("Z1")
    return head_type


def convert_list_to_zlist(the_list, head_type):
    """Turns a Python iterable into a Typed List.

    Arguments:
        the_list: an iterable of serialized ZObjects

    Returns:
        a Type List corresponding to the input list
    """
    list_type = {
        "Z1K1": wrap_in_z9("Z7"),
        "Z7K1": wrap_in_z9("Z881"),
        "Z881K1": head_type,
    }

    head_key = "K1"
    tail_key = "K2"

    def create_tail():
        return {"Z1K1": list_type}

    result = create_tail()
    tail = result
    for element in the_list:
        tail[head_key] = element
        tail[tail_key] = create_tail()
        tail = tail[tail_key]
    return result


def get_zid(Z4):
    if _is_zfunction(Z4):
        return get_zid(Z4.get("Z8K5", {}))
    if _is_zreference(Z4):
        return Z4["Z9K1"]
    if _is_zfunction_call(Z4):
        Z7K1 = Z4.get("Z7K1", {})
        return get_zid(Z7K1)
    if is_ztype(Z4):
        return get_zid(Z4["Z4K1"])
    if isinstance(Z4, str):
        # If Z4 is a string, original object was a Z6 or a Z9.
        return Z4
    # I guess this wasn't a very good ZObject.
    raise exceptions.EvaluatorError("Could not determine type for {}".format(Z4))


def get_zobject_type(ZObject):
    """Determine the ZID corresponding to the type of a ZObject."""
    return get_zid(ZObject.get("Z1K1"))


def is_ztype(Z4):
    try:
        return Z4.get("Z1K1", {}).get("Z9K1") == "Z4"
    except AttributeError:
        return False


def get_python_type(py_object):
    """Infer the type of a Python object and try to find the corresponding ZID."""
    if isinstance(py_object, ztypes.ZObject):
        return "DEFAULT"
    if isinstance(py_object, str):
        return "Z6"
    if isinstance(py_object, ztypes.ZReference):
        return "Z9"
    if isinstance(py_object, bool):
        return "Z40"
    if isinstance(py_object, ztypes.ZPair):
        return "Z882"
    if isinstance(py_object, dict):
        return "Z883"
    # This check crucially must succeed the isinstance(py_object, ZObject) check
    # because ZObjects are sort of iterable.
    try:
        _ = iter(py_object)
    except TypeError:
        pass
    else:
        return "Z881"
    if py_object is None:
        return "Z21"

'use strict';

const Server = require( '../../utils/server.js' );
const { executorClassTest } = require( '../../utils/integrationTest.js' );
const { javascriptExecutorInterfaceTestData } = require( '../../utils/data.js' );
const { Evaluator } = require( '../../../src/Evaluator.js' );

describe( 'executor-interface', function () {

	this.timeout( 20000 );

	const server = new Server();
	const testRunner = function ( testName, callBack ) {
		it( testName, callBack );
	};

	before( async () => {
		await server.start();
	} );

	after( async () => {
		server.stop();
		// This is necessary to ensure that all spawned processes are killed;
		// without this, CI runners will wait forever because the image still
		// has active processes. If only JS had destructors ...
		await Evaluator.drainTheSwamps();
	} );

	for ( const testData of javascriptExecutorInterfaceTestData ) {
		executorClassTest(
			testData.environment || {},
			testRunner,
			testData.name,
			testData.input,
			testData.expectedOutput,
			testData.expectedErrorType,
			testData.expectedErrorKeyPhrase,
			testData.expectedExtraMetadata,
			testData.expectedMissingMetadata,
			testData.expectedLogs || []
		);
	}

} );

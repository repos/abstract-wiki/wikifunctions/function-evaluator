'use strict';

const os = require( 'os' );
const { cpuUsage, memoryUsage } = require( 'node:process' );

const { ExecutorPool } = require( './ExecutorPool.js' );
const { ExecutorRegistry } = require( './ExecutorRegistry' );
const { getTimeoutForEnvironment } = require( './environmentUtils.js' );

const { error, makeErrorInNormalForm } = require( '../executors/javascript-wasmedge/function-schemata/javascript/src/error.js' );
const { makeMappedResultEnvelope, setMetadataValues, wrapInZ6 } = require( '../executors/javascript-wasmedge/function-schemata/javascript/src/utils.js' );

// Executor registration happens upon import/require.
require( '../registerExecutors.js' );

let globalEvaluator;

/**
 * The Evaluator; that gets called by function-orchestrator when evaluate() gets invoked.
 */
class Evaluator {

	constructor( executorPools, codingLanguageToPoolIndex, environment ) {
		this.executorPools_ = executorPools;
		this.codingLanguageToPoolIndex_ = codingLanguageToPoolIndex;
		this.environment_ = environment;
	}

	static evaluatorInstances_ = [];

	static register( evaluatorInstance ) {
		Evaluator.evaluatorInstances_.push( evaluatorInstance );
	}

	/**
	 * Utility function used in many of our tests to ensure that all spawned processes are killed;
	 * without this, CI runners will wait forever because the image still has active processes.
	 *
	 */
	static async drainTheSwamps() {
		const drainingSwamps = [];
		for ( const instance of Evaluator.evaluatorInstances_ ) {
			drainingSwamps.push( instance.drainTheSwamp() );
		}
		await Promise.all( drainingSwamps );
	}

	/**
	 * Creates an Evaluator instance for the given environment, e.g. 'production'
	 *
	 * @param {string} environment
	 * @param {Object} logger singleton logger instance to use in all Executors
	 * @return {Object} instance the newly created Evaluator instance
	 */
	static create( environment, logger ) {
		const executorPools = ExecutorRegistry.executorClasses.map(
			( executorClass ) => new ExecutorPool( executorClass, environment, logger ) );
		const codingLanguageToPoolIndex = new Map(
			ExecutorRegistry.codingLanguageToClassIndex.entries() );
		const instance = new Evaluator( executorPools, codingLanguageToPoolIndex, environment );
		Evaluator.register( instance );
		return instance;
	}

	static setGlobalEvaluator( evaluatorInstance ) {
		globalEvaluator = evaluatorInstance;
	}

	static getGlobalEvaluator() {
		return globalEvaluator;
	}

	async getExecutorFor( codingLanguage ) {
		const index = this.codingLanguageToPoolIndex_.get( codingLanguage );
		if ( index === undefined ) {
			// This case is handled in routes/evaluate.js.
			return null;
		}
		return await ( this.executorPools_[ index ].getExecutor() );
	}

	/**
	 * Main evaluator function;
	 * that takes in the Z7 request to executor to fetch the evaluated result.
	 *
	 * @param {Object} functionCallRequest a zobject, in this case Z7 Object
	 * @param {Object} webSocketServer the WebSocket instance used to call the Executor
	 * @return {Object} result the fetched result from Promise
	 */
	async evaluate( functionCallRequest, webSocketServer ) {

		// Handle timeouts: this Promise will return first if function
		// execution takes longer than the timeout limit.
		const timeoutLimit = getTimeoutForEnvironment( this.environment_ );
		let timer = null;
		const timerPromise = new Promise( ( resolve ) => {
			timer = setTimeout(
				() => {
					resolve(
						makeMappedResultEnvelope(
							null,
							makeErrorInNormalForm(
								error.evaluator_time_limit,
								[ wrapInZ6( timeoutLimit.toString() + ' ms' ) ]
							)
						)
					);
				},
				timeoutLimit
			);
		} );

		// Start measuring time and CPU usage.
		const startTime = new Date();
		const startUsage = cpuUsage();

		// Get an Executor instance and start running the function call.
		// This Promise will run in parallel to the timeout Promise.
		let executor = null;
		const executorFunction = async () => {
			// Everything is apparently fine, so see if the coding language has an executor.
			const codingLanguage = functionCallRequest.codingLanguage;
			executor = await this.getExecutorFor( codingLanguage );
			if ( executor === null ) {
				return makeMappedResultEnvelope(
					null,
					makeErrorInNormalForm(
						error.invalid_programming_language,
						[ functionCallRequest.codingLanguage ]
					)
				);
			}

			// Execute the function call.
			let Z22;
			if ( functionCallRequest.reentrant ) {
				webSocketServer.once( 'connection', async ( webSocket ) => {
					Z22 = await executor.execute( functionCallRequest, webSocket );
				} );
			} else {
				Z22 = await executor.execute( functionCallRequest, /* webSocket= */ null );
			}
			return Z22;
		};
		const executorPromise = executorFunction();

		// This try/finally block is crucial. It's essential that, no matter
		// what happens, the executor resolve its internal Promise. This effectively
		// releases a lock so that another Executor can start its internal process.
		let result = null;
		try {
			result = await Promise.race( [ timerPromise, executorPromise ] );
		} finally {
			if ( executor !== null ) {
				await executor.immaHeadOut();
			}
			if ( timer !== null ) {
				clearTimeout( timer );
			}
		}

		// Populate metadata related to time, CPU usage, memory usage, and hostname.
		const cpuUsageStats = cpuUsage( startUsage );
		const cpuUsageStr = ( ( cpuUsageStats.user + cpuUsageStats.system ) / 1000 ) + ' ms';
		const memoryUsageStr = Math.round( memoryUsage.rss() / 1024 / 1024 * 100 ) / 100 + ' MiB';
		const endTime = new Date();
		const startTimeStr = startTime.toISOString();
		const endTimeStr = endTime.toISOString();
		const durationStr = ( endTime.getTime() - startTime.getTime() ) + ' ms';
		const hostname = os.hostname();
		const metadataValues = [
			[ 'evaluationMemoryUsage', memoryUsageStr ],
			[ 'evaluationCpuUsage', cpuUsageStr ],
			[ 'evaluationStartTime', startTimeStr ],
			[ 'evaluationEndTime', endTimeStr ],
			[ 'evaluationDuration', durationStr ],
			[ 'evaluationHostname', hostname ],
			[ 'evaluationMemoryUsage', memoryUsageStr ]
		].map( ( keyAndValue ) => keyAndValue.map( wrapInZ6 ) );
		setMetadataValues( result, metadataValues );

		return result;
	}

	async drainTheSwamp() {
		const drainingPools = [];
		for ( const pool of this.executorPools_ ) {
			drainingPools.push( pool.drain() );
		}
		await Promise.all( drainingPools );
	}

}

module.exports = { Evaluator };

'use strict';

const pidusage = require( 'pidusage' );
const { spawn } = require( 'child_process' );

const { error, makeErrorInNormalForm } = require( '../executors/javascript-wasmedge/function-schemata/javascript/src/error.js' );
const {
	convertItemArrayToZList,
	isString,
	makeMappedResultEnvelope,
	setMetadataValue,
	setMetadataValues,
	wrapInZ6
} = require( '../executors/javascript-wasmedge/function-schemata/javascript/src/utils.js' );

const {
	getWasmedgeGasLimitForEnvironment,
	getWasmedgeMemoryPageLimitForEnvironment,
	getWasmedgeTimeoutForEnvironment
} = require( './environmentUtils.js' );
const { AttemptedLogger } = require( './AttemptedLogger.js' );

const START_RESULT_REGEX_ = /^start result <<<$/;
const START_DEBUG_LOG_REGEX_ = /^start debug log <<<$/;
const START_INTERNAL_LOG_REGEX_ = /^start internal log <<<$/;
const START_REENTRANT_CALL_REGEX_ = /^start reentrant call <<<$/;
const END_RESULT_REGEX_ = /^>>> end result$/;
const END_DEBUG_LOG_REGEX_ = /^>>> end debug log$/;
const END_INTERNAL_LOG_REGEX_ = /^>>> end internal log$/;
const END_REENTRANT_CALL_REGEX_ = /^>>> end reentrant call$/;

// wasmedge statistics: gas cost.
const GAS_REGEX_ = /.*Gas costs: (\d*)$/;
// wasmedge statistics: total execution time.
const EXECUTION_TIME_REGEX_ = /.*Total execution time: (\d*) ns$/;

const WASMEDGE_STATISTICS_REGEX_ = /\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3}\] \[info\]/;
const WASMEDGE_ERROR_REGEX_ = /\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3}\] \[error\]/;

const ParseAction = Object.freeze( {
	EMPTY: Symbol( 'ParseAction.EMPTY' ),
	START_DEBUG_LOG: Symbol( 'ParseAction.START_DEBUG_LOG' ),
	START_INTERNAL_LOG: Symbol( 'ParseAction.START_INTERNAL_LOG' ),
	START_REENTRANT_CALL: Symbol( 'ParseAction.START_REENTRANT_CALL' ),
	START_RESULT: Symbol( 'ParseAction.START_RESULT' )
} );

const ParseState = Object.freeze( {
	EMPTY: Symbol( 'ParseState.EMPTY' ),
	AWAITING_DEBUG_LOG: Symbol( 'ParseState.AWAITING_DEBUG_LOG' ),
	AWAITING_INTERNAL_LOG: Symbol( 'ParseState.AWAITING_INTERNAL_LOG' ),
	AWAITING_REENTRANT_CALL: Symbol( 'ParseState.AWAITING_REENTRANT_CALL' ),
	AWAITING_RESULT: Symbol( 'ParseState.AWAITING_RESULT' )
} );

function processWasmedgeStatistics( statisticsQueue ) {
	// Remaining stdout pertains to wasmedge usage statistics.
	const wasmResourceUsage = new Map();
	const metadataToRegex = new Map( [
		[ 'wasmedgeGasCost', GAS_REGEX_ ],
		[ 'wasmedgeTotalExecutionTime', EXECUTION_TIME_REGEX_ ]
	] );
	for ( const data of statisticsQueue ) {
		for ( const entry of metadataToRegex.entries() ) {
			const [ metadataKey, regex ] = entry;
			const match = data.match( regex );
			if ( match !== null ) {
				wasmResourceUsage.set( metadataKey, match[ 1 ] );
				break;
			}
		}
	}
	return wasmResourceUsage;
}

function populateMetadataMap( debugLogQueue, logger ) {
	const metadataMap = new Map();
	for ( const log of debugLogQueue ) {
		let payload;
		try {
			payload = JSON.parse( log );
		} catch {
			// Skip further processing but emit a debug log.
			logger.log( 'warn',
				{ message: `Could not parse debug log: ${ log }` }
			);
			continue;
		}
		let { key, logString } = payload;
		if ( key === undefined || logString === undefined ) {
			// Skip further processing but emit a debug log.
			logger.log( 'info',
				{ message: `Log did not contain expected keys {key, logString}: ${ log }` }
			);
			continue;
		}
		// TODO (T382502): These calls to JSON.stringify can produce
		// really uninformative and surprising outputs; consider an
		// alternative. Ideally, we would be strict here and raise a
		// specific error type in this case.
		if ( !isString( key ) ) {
			key = JSON.stringify( key );
		}
		if ( !isString( logString ) ) {
			logString = JSON.stringify( logString );
		}
		if ( !metadataMap.has( key ) ) {
			metadataMap.set( key, [] );
		}
		metadataMap.get( key ).push( logString );
	}
	return metadataMap;
}

class QueueByLogging {

	constructor( logger, requestId ) {
		this.logger_ = logger;
		this.requestId_ = requestId;
	}

	push( message ) {
		this.logger_.log( 'info', { message: message, requestId: this.requestId_ } );
	}

}

// TODO (T365515): Try using `tree-kill` or similar instead of rolling our own
// brittle version of process tree termination.
async function getLeavesOfProcessTree( originalPid ) {
	const leafyBabies = [];
	const searchQueue = [ originalPid ];

	const visitNode = ( pid ) => {
		leafyBabies.push( pid );
	};

	const searchNode = async () => {
		const nextPid = searchQueue.shift();
		if ( nextPid === undefined ) {
			return;
		}
		let doVisit = true;
		const pgrepProcess = spawn( 'pgrep', [ '-P', nextPid ], {
			detached: false,
			shell: false
		} );
		const processEndPromise = new Promise( ( resolve ) => {
			pgrepProcess.stdout.on( 'data', ( data ) => {
				const allData = data.toString().trim();
				for ( const pid of allData.split( '\n' ) ) {
					doVisit = false;
					searchQueue.push( pid );
				}
			} );
			pgrepProcess.on( 'close', () => {
				resolve();
			} );
		} );
		if ( doVisit ) {
			visitNode( nextPid );
		}
		await processEndPromise;
	};

	while ( searchQueue.length > 0 ) {
		await searchNode();
	}

	return leafyBabies;
}

// TODO (T365515): Try using `tree-kill` or similar instead of rolling our own
// brittle version of process tree termination.
async function killProcessFamily( pid ) {
	const pidsToKill = await getLeavesOfProcessTree( pid );
	for ( const pid of pidsToKill ) {
		try {
			process.kill( pid );
		} catch ( error ) {}
	}
}

/**
 * The Executor;
 * that gets called by the Evaluator to execute the given function call (Z7) object
 */
class Executor {

	constructor( logger ) {
		this.childProcess_ = null;
		Object.defineProperty( this, 'childProcess', {
			get: function () {
				return this.childProcess_;
			}
		} );
		// TODO (T382447): Avoid use of AttemptedLogger if possible.
		this.logger_ = new AttemptedLogger( logger );

		// Signal that this executor's process has been killed so another
		// is free to be spawned.
		const instance = this;
		this.processIsDead = new Promise( ( resolve ) => {
			instance.obituary_ = resolve;
		} );

		// Capture stray output like wasmedge statistics if execution
		// completes successfully.
		this.defaultQueue_ = [];
		// Capture the resultant ZObject.
		this.resultQueue_ = [];
		// Capture debug logs, which will become individual metadata items.
		this.debugLogQueue_ = [];
		// Log internal logs whenever they are found.
		this.internalLogQueue_ = null;
		// Capture requests for reentrant calls, which will communicate back
		// to the orchestrator via websocket.
		this.reentrantCallQueue_ = [];

		// Manage queues based on signals sent in stdout.
		this.defaultStateAndQueue_ = [ ParseState.EMPTY, this.defaultQueue_ ];
		this.currentStateAndQueue_ = this.defaultStateAndQueue_;
	}

	end( resolve ) {
		this.childProcess_.stdin.end();
		process.kill( this.childProcess_.pid );
		resolve();
	}

	getVersionString() {
		return this.versionString || null;
	}

	async waitForProcess() {
		while ( true ) {
			if ( this.childProcess_ !== null ) {
				return;
			}
		}
	}

	getStartAction( data ) {
		if ( data.match( START_RESULT_REGEX_ ) ) {
			return ParseAction.START_RESULT;
		}
		if ( data.match( START_DEBUG_LOG_REGEX_ ) ) {
			return ParseAction.START_DEBUG_LOG;
		}
		if ( data.match( START_INTERNAL_LOG_REGEX_ ) ) {
			return ParseAction.START_INTERNAL_LOG;
		}
		if ( data.match( START_REENTRANT_CALL_REGEX_ ) ) {
			return ParseAction.START_REENTRANT_CALL;
		}
		return null;
	}

	processStartAction( startAction ) {
		const currentState = this.currentStateAndQueue_[ 0 ];
		if ( currentState !== ParseState.EMPTY ) {
			const errorString = 'Action ' + startAction.toString() + ' interrupted ' + currentState.toString();
			return makeMappedResultEnvelope(
				null,
				makeErrorInNormalForm(
					error.invalid_executor_response,
					[ wrapInZ6( errorString ) ]
				)
			);
		}
		if ( startAction === ParseAction.START_DEBUG_LOG ) {
			this.currentStateAndQueue_ = [ ParseState.AWAITING_DEBUG_LOG, this.debugLogQueue_ ];
		} else if ( startAction === ParseAction.START_INTERNAL_LOG ) {
			this.currentStateAndQueue_ = [
				ParseState.AWAITING_INTERNAL_LOG, this.internalLogQueue_ ];
		} else if ( startAction === ParseAction.START_RESULT ) {
			this.currentStateAndQueue_ = [ ParseState.AWAITING_RESULT, this.resultQueue_ ];
		} else if ( startAction === ParseAction.START_REENTRANT_CALL ) {
			this.currentStateAndQueue_ = [
				ParseState.AWAITING_REENTRANT_CALL, this.reentrantCallQueue_ ];
		} else {
			return makeMappedResultEnvelope(
				null,
				makeErrorInNormalForm(
					error.invalid_executor_response,
					[ wrapInZ6( 'invalid start action' ) ]
				)
			);
		}
		return null;
	}

	processNonEmpty( data, websocket ) {
		const [ currentState, currentQueue ] = this.currentStateAndQueue_;
		if (
			(
				currentState === ParseState.AWAITING_DEBUG_LOG &&
                data.match( END_DEBUG_LOG_REGEX_ ) ) ||
			(
				currentState === ParseState.AWAITING_INTERNAL_LOG &&
                data.match( END_INTERNAL_LOG_REGEX_ ) ) ||
			(
				currentState === ParseState.AWAITING_RESULT &&
				data.match( END_RESULT_REGEX_ ) ) ) {
			this.currentStateAndQueue_ = this.defaultStateAndQueue_;
		} else if (
			(
				currentState === ParseState.AWAITING_REENTRANT_CALL &&
                data.match( END_REENTRANT_CALL_REGEX_ ) ) ) {
			this.currentStateAndQueue_ = this.defaultStateAndQueue_;
			if ( websocket === null ) {
				return makeMappedResultEnvelope(
					null,
					makeErrorInNormalForm(
						error.call_by_non_reentrant_executor,
						[ wrapInZ6( data ) ]
					)
				);
			} else {
				const reentrantString = this.reentrantCallQueue_.join( '\n' );
				this.reentrantCallQueue_ = [];

				try {
					JSON.parse( reentrantString );
				} catch {
					this.logger_.log( 'warn', { message: `Could not parse sub-call: ${ reentrantString }` } );
				}
				// TODO (T321866): Make reentrant call to the orchestrator.
			}
		} else {
			currentQueue.push( data );
		}
		return null;
	}

	processEmpty( data ) {
		const startAction = this.getStartAction( data );
		if ( startAction !== null ) {
			return this.processStartAction( startAction );
		}
		// Non-start output sent during state EMPTY is hopefully a wasmedge
		// statistic or an error message due to resource exhaustion.
		const currentQueue = this.currentStateAndQueue_[ 1 ];
		currentQueue.push( data );
		return null;
	}

	// stdout from executors can be captured by the following CFG†:
	//
	// <STDOUT> := <ACTION>* <STATISTIC>*
	// <ACTION> := <RESULT> | <REENTRANT_CALL> | <DEBUG_LOG> | <INTERNAL_LOG>
	// <RESULT> := "start result <<<" Z22 ">>> end result"
	// <REENTRANT_CALL> := "start reentrant call <<<" Z7 ">>> end reentrant call"
	// <DEBUG_LOG> := "start debug log <<<" .* ">>> end debug log"
	// <INTERNAL_LOG> := "start internal log <<<" .* ">>> end internal log"
	// <STATISTIC> := .* ( "Gas costs" | "Total execution time" ) .*
	//
	// † what is implemented is actually a state machine, but it's easier to
	// express what's happening with CFG syntax. This grammar does not account
	// for the possibility of error logs due to violation of wasmedge
	// resource limits, which can happen at any point, conceivably interrupting
	// any element of the grammar above at any point.
	// †† other statistics than the ones mentioned above are reported but
	// will be ignored.
	processFunctionCallOutput( data, websocket ) {
		const currentState = this.currentStateAndQueue_[ 0 ];
		if ( currentState === undefined ) {
			// This should never happen.
			return makeMappedResultEnvelope(
				null,
				makeErrorInNormalForm(
					error.invalid_executor_response,
					[ wrapInZ6( 'no current state' ) ]
				)
			);
		}
		if ( currentState === ParseState.EMPTY ) {
			return this.processEmpty( data );
		} else {
			return this.processNonEmpty( data, websocket );
		}
	}

	startExecutorSubprocess( options ) {
		const wasmCommandParts = [
			'wasmedge',
			'--enable-all-statistics',
			this.constructor.directoryMapping
		];
		const environment = options.env;
		if ( environment.ENABLE_WASMEDGE_LIMITS === '1' ) {
			const timeLimit = getWasmedgeTimeoutForEnvironment( environment );
			wasmCommandParts.push( `--time-limit ${ timeLimit }` );
			const gasLimit = getWasmedgeGasLimitForEnvironment( environment );
			wasmCommandParts.push( `--gas-limit ${ gasLimit }` );
			const memoryPageLimit = getWasmedgeMemoryPageLimitForEnvironment( environment );
			wasmCommandParts.push( `--memory-page-limit ${ memoryPageLimit }` );
		}
		wasmCommandParts.push( this.constructor.runTheBinary );

		this.childProcess_ = spawn( wasmCommandParts.join( ' ' ), options );
		this.childProcess_.unref();
		this.childProcess_.stdin.setEncoding( 'utf-8' );
	}

	async runExecution( functionCallRequest, websocket = null ) {
		let Z22 = null;

		const processEndPromise = new Promise( ( resolve ) => {
			this.childProcess_.on( 'close', () => {
				resolve();
			} );
			this.childProcess_.stdout.on( 'data', ( allTheData ) => {
				// TODO (T295699): Avoid toString; find a way to merge Buffers.
				const dataParts = allTheData.toString().split( '\n' );
				for ( const data of dataParts ) {
					const badZ22 = this.processFunctionCallOutput( data, websocket );
					// Data processing may return an irregular Z22. In this case,
					// abort execution immediately and set the Z22.
					if ( badZ22 !== null ) {
						Z22 = badZ22;
						this.end( resolve );
					}
				}
			} );
		} );

		if ( websocket !== null ) {
			websocket.on( 'message', ( message ) => {
				this.childProcess_.stdin.write( message + '\n' );
			} );
		}

		// Write ZObject to executor process.
		// Two newlines seem necessary for QuickJS's polling to be able to read
		// from the stream.
		this.childProcess_.stdin.write( JSON.stringify( functionCallRequest ) + '\n\n' );

		// Let it run.
		await processEndPromise;

		// Collect wasmedge statistics and, if applicable, error states from
		// the last-used queue. This could be any queue because wasmedge's
		// resource limits might cause abnormal exit at any point during
		// execution.
		const currentQueue = this.currentStateAndQueue_[ 1 ];
		const wasmedgeStatisticsQueue = [];
		const wasmedgeErrorQueue = [];
		const remainingQueue = [];
		while ( true ) {
			if ( !( currentQueue.length > 0 ) ) {
				break;
			}
			// We process the last-used queue in reverse order because,
			// conceivably, a portion of the output might match the regexes.
			// By processing in reverse order, we can guarantee that we consume
			// all of the wasmedge stdout before attempting to process the
			// executors' stdout.
			const lastElement = currentQueue.pop();

			// Last elements should be wasmedge statistics.
			if ( lastElement.match( WASMEDGE_STATISTICS_REGEX_ ) ) {
				wasmedgeStatisticsQueue.unshift( lastElement );
				continue;
			}
			// Optionally, there may be wasmedge error elements.
			if ( lastElement.match( WASMEDGE_ERROR_REGEX_ ) ) {
				wasmedgeErrorQueue.unshift( lastElement );
				continue;
			}
			// Anything remaining is potentially part of some useful output,
			// so store it elsewhere.
			remainingQueue.unshift( lastElement );
		}

		// Z22 may already have been set to an error state. If it hasn't, read
		// the Z22 returned by the executor (stored in this.resultQueue_) or
		// generate an error.
		if ( Z22 === null ) {
			let makeErrorFromContents = false;
			const contents = this.resultQueue_.join( '\n' );

			if ( contents ) {
				try {
					const parsedContents = JSON.parse( contents );
					Z22 = makeMappedResultEnvelope( ...parsedContents );
				} catch ( error ) {
					makeErrorFromContents = true;
				}
			} else {
				const wasmedgeErrors = wasmedgeErrorQueue.join( '\n' );
				if ( wasmedgeErrors.trim() ) {
					Z22 = makeMappedResultEnvelope(
						null,
						makeErrorInNormalForm(
							error.invalid_executor_response,
							[ wrapInZ6( wasmedgeErrors ) ]
						)
					);
				} else {
					makeErrorFromContents = true;
				}
			}

			if ( makeErrorFromContents ) {
				Z22 = makeMappedResultEnvelope(
					null,
					makeErrorInNormalForm(
						error.invalid_executor_response,
						[ wrapInZ6( contents ) ]
					)
				);
			}
		}

		// Populate debug logs metadata.
		const metadataMap = populateMetadataMap( this.debugLogQueue_, this.logger_ );
		for ( const key of metadataMap.keys() ) {
			const wrappedKey = wrapInZ6( key );
			const metadataValues = convertItemArrayToZList(
				metadataMap.get( key ).map( wrapInZ6 ) );
			setMetadataValue( Z22, wrappedKey, metadataValues );
		}

		// Populate wasmedge statistics metadata.
		const wasmResourceUsage = processWasmedgeStatistics( wasmedgeStatisticsQueue );
		for ( const entry of wasmResourceUsage.entries() ) {
			const [ metadataKey, metadataValue ] = entry;
			setMetadataValue( Z22, wrapInZ6( metadataKey ), wrapInZ6( metadataValue ) );
		}
		return Z22;
	}

	/**
	 * Primary Executor function;
	 * that gets invoked by the Evaluator to retrieve the evaluation on a given Z7 object
	 *
	 * @param {Object} functionCallRequest the zobject: a Z7 object
	 * @param {Object|null} websocket the Web Socket instance, otherwise null
	 * @return {Object} Z22 a zobject, the evaluated result
	 */
	async execute( functionCallRequest, websocket = null ) {

		this.internalLogQueue_ = new QueueByLogging(
			this.logger_, functionCallRequest.requestId );
		this.logger_.log( 'info',
			{ message: 'calling execute in evaluator...', requestId: functionCallRequest.requestId }
		);

		const Z22Promise = this.runExecution( functionCallRequest, websocket );
		const pidStatsPromise = this.waitForProcess().then(
			async () => await pidusage( this.childProcess_.pid )
		);

		const Z22 = await ( Z22Promise );

		this.logger_.log( 'info',
			{ message: '...finished calling execute in evaluator', requestId: functionCallRequest.requestId }
		);

		let pidStats, pidStatsErrorString;
		try {
			pidStats = await pidStatsPromise;
		} catch ( e ) {
			pidStats = null;
			pidStatsErrorString = e.toString();
		}

		// Wait until subprocess exits; return the result of function execution.
		pidusage.clear();
		let execMemoryUsageStr;
		let execCpuUsageStr;
		if ( pidStats !== null ) {
			execMemoryUsageStr = Math.round( pidStats.memory / 1024 / 1024 * 100 ) / 100 + ' MiB';
			const execCpuUsage = pidStats.elapsed;
			// Don't give a meaningless execution CPU usage if it's less than the timing attack val.
			execCpuUsageStr = ( execCpuUsage < 50 ? '<50' : execCpuUsage.toString() ) + ' μs';
		} else {
			execMemoryUsageStr = pidStatsErrorString;
			execCpuUsageStr = pidStatsErrorString;
		}

		const rawMetadata = [
			[ 'executionMemoryUsage', execMemoryUsageStr ],
			[ 'executionCpuUsage', execCpuUsageStr ]
		];
		const versionString = this.getVersionString();
		rawMetadata.push( [ 'programmingLanguageVersion', versionString ] );

		const metadataValues = rawMetadata.map(
			( keyAndValue ) => keyAndValue.map( wrapInZ6 ) );
		setMetadataValues( Z22, metadataValues );

		return Z22;
	}

	async immaHeadOut() {
		// Kill the executor child process if it has survived.
		if ( this.childProcess_ !== null && !this.childProcess_.killed ) {
			await killProcessFamily( this.childProcess_.pid );
		}

		// Announce the death of the child.
		this.obituary_();
	}

}

module.exports = { Executor };

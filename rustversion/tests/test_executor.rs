#[cfg(test)]
mod executor_tests {

    use std::fs::read_to_string;
    use function_evaluator::executor::{AcceptedErrors, Executor};
    use function_evaluator::executor_classes::{
        ExecutorWasiBuilder,
        JavaScriptWasiBuilder,
        PythonWasiBuilder
    };

    static JAVASCRIPT_INTERPRETER_FILE: &str = "./interpreters/wasip1_quickjs_uncompiled_spliced.wasm";
    static PYTHON_INTERPRETER_FILE: &str = "./interpreters/wasip1_rustpython_uncompiled.wasm";

    fn read_file_as_json(file_name: &str) -> serde_json::Value {
        let file_contents_result = read_to_string(file_name);
        assert!(file_contents_result.is_ok());
        let file_contents = file_contents_result.unwrap();
        let deserialized_result = serde_json::from_str(file_contents.as_str());
        assert!(deserialized_result.is_ok());
        let deserialized = deserialized_result.unwrap();
        return deserialized;
    }

    async fn run_test<T: ExecutorWasiBuilder>(input_file_name: &str, interpreter_file_name: &str, expected_output_file_name: &str) -> Result<(), AcceptedErrors> {
        // Serialize to a single line to facilitate passing over stdin.
        let deserialized = read_file_as_json(input_file_name);
        let test_payload_result = serde_json::to_string(&deserialized);
        assert!(test_payload_result.is_ok());
        let test_payload = test_payload_result.unwrap();

        // Create the Executor.
        let mut executor = Executor::new::<T>(interpreter_file_name);

        // Execute.
        let stringified_execution_result = executor.execute(test_payload).await?;

        // Compare the results.
        let actual_result: serde_json::Value = serde_json::from_str(
            &stringified_execution_result).unwrap();
        let expected_result = read_file_as_json(expected_output_file_name);
        assert_eq!(actual_result[0], expected_result["Z22K1"]);
        Ok(())
    }

    async fn run_python_test(input_file_name: &str, expected_output_file_name: &str) -> Result<(), AcceptedErrors> {
        run_test::<PythonWasiBuilder>(input_file_name, PYTHON_INTERPRETER_FILE, expected_output_file_name).await
    }

    async fn run_javascript_test(input_file_name: &str, expected_output_file_name: &str) -> Result<(), AcceptedErrors> {
        run_test::<JavaScriptWasiBuilder>(input_file_name, JAVASCRIPT_INTERPRETER_FILE, expected_output_file_name).await
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_python3_add() {
        let test_ran = run_python_test(
            "../test_data/python3_add.json",
            "../test_data/add_expected.json").await;
        println!("{:?}", test_ran);
        assert!(test_ran.is_ok());
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_python3_add_lambda() {
        let test_ran = run_python_test(
            "../test_data/python3_add_lambda.json",
            "../test_data/add_expected.json").await;
        assert!(test_ran.is_ok());
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_python3_list_of_type_converters() {
        let test_ran = run_python_test(
            "../test_data/python3_list_of_type_converters.json",
            "../test_data/python3_list_of_type_converters_expected.json").await;
        assert!(test_ran.is_ok());
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_javascript_add() {
        let test_ran = run_javascript_test(
            "../test_data/javascript_add.json",
            "../test_data/add_expected.json").await;
        println!("{:?}", test_ran);
        assert!(test_ran.is_ok());
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_javascript_add_with_type_converters() {
        let test_ran = run_javascript_test(
            "../test_data/javascript_add_with_type_converters.json",
            "../test_data/add_Z10000_expected.json").await;
        println!("{:?}", test_ran);
        assert!(test_ran.is_ok());
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_javascript_regexes() {
        let test_ran = run_javascript_test(
            "../test_data/javascript_regexes.json",
            "../test_data/regexes_expected.json").await;
        assert!(test_ran.is_ok());
    }

}

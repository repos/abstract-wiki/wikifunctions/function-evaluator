enum ParseState {
    Empty,
    AwaitingResult,
    GotResult
}

pub struct StateMachine {
    current_state: ParseState,
    result_string: Option<String>
}

impl StateMachine {

    pub fn new() -> Self {
        Self{
            current_state: ParseState::Empty,
            result_string: None
        }
    }

    fn process_empty( &mut self, message: &str ) {
        if message == "start result <<<" {
            self.current_state = ParseState::AwaitingResult;
        }
    }

    fn process_result( &mut self, message: &str ) {
        self.result_string = Some( String::from( message ) );
        self.current_state = ParseState::GotResult;
    }

    pub fn process_message( &mut self, message: &str ) {
        println!("process_message called with {}", message);
        match self.current_state {
            ParseState::Empty => self.process_empty( message ),
            ParseState::AwaitingResult => self.process_result( message ),
            ParseState::GotResult => {}
        }
    }

    pub fn result_string_moved( &mut self ) -> Option<String> {
        std::mem::take( &mut self.result_string )
    }

}

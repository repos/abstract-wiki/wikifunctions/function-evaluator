'use strict';

const Benchmark = require( 'benchmark' );
const { Evaluator } = require( '../../src/Evaluator.js' );
const fetch = require( '../../lib/fetch.js' );
const { convertFormattedRequestToVersionedBinary, convertWrappedZObjectToVersionedBinary } =
	require( '../../executors/javascript-wasmedge/function-schemata/javascript/src/serialize.js' );
const Server = require( '../utils/server.js' );

// Default benchmark options.
const defaultOptions = {
	defer: true, // for async calls.
	minSamples: 20, // ensure the result stability.
	initCount: 2 // default is one if not set.
};

/**
 * Evaluates function call and benchmarks running time.
 *
 * @param {string} name The name of the test and benchmark.
 * @param {Object} functionCall The input object.
 * @param {string} useBinaryFormatVersion boolean
 */
/* eslint-disable no-unused-vars */
function addBenchmarkTest( name, functionCall, useBinaryFormatVersion, benchmarkSuite, uri ) {
	// copied from integrationTest.js
	let binaryBlob;
	let schemaVersion = '0.1.0';
	if ( useBinaryFormatVersion !== undefined ) {
		schemaVersion = useBinaryFormatVersion;
		binaryBlob = convertFormattedRequestToVersionedBinary( functionCall, schemaVersion );
	} else {
		const wrappedInput = {
			reentrant: false,
			zobject: functionCall,
			remainingTime: 15,
			// Request IDs must be distinct.
			requestId: 'thebestrequestid' + name
		};
		binaryBlob = convertWrappedZObjectToVersionedBinary( wrappedInput, schemaVersion );
	}

	// Run inside the benchmark suite.
	benchmarkSuite.add(
		name + ' benchmark',
		async ( deferred ) => {
			await fetch(
				uri,
				{
					method: 'POST',
					body: binaryBlob,
					headers: { 'Content-type': 'application/octet-stream' }
				}
			);
			deferred.resolve();
		},
		defaultOptions
	);
}

function runBenchmarkTests( testDataArray ) {

	let benchmarkSuite = null;
	let uri = null;

	const server = new Server();
	server.start().then( () => {
		uri = `${ server.config.uri }wikifunctions.org/v1/evaluate/`;
		benchmarkSuite = new Benchmark.Suite(
			'evaluator benchmark suite',
			{
				onCycle: ( event ) => {
					// This gets called between benchmarks.
					console.log( String( event.target ) );
				},

				onComplete: async ( event ) => {
					await Evaluator.drainTheSwamps();
					server.stop();
				}
			}
		);

		for ( const testData of testDataArray ) {
			if ( !testData.skipBenchmark ) {
				addBenchmarkTest(
					testData.name, testData.input, testData.useBinaryFormatVersion,
					benchmarkSuite, uri
				);
			}
		}

		benchmarkSuite.run( { async: true } );
	} );

}

module.exports = { runBenchmarkTests };

import { runFunction as runFunction } from './executor.js';
import * as std from 'std'; // eslint-disable-line n/no-missing-import
import { readFromStream } from './utils.js';

function readFromStdin() {
	const result = readFromStream( std.in );
	std.in.flush();
	return result;
}

function writeToStdout( contents ) {
	print( contents ); // eslint-disable-line no-undef
	std.out.flush();
}

runFunction( readFromStdin, writeToStdout );

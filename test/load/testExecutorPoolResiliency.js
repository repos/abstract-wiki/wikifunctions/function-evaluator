'use strict';

const assert = require( '../utils/assert.js' );
const Server = require( '../utils/server.js' );
const fetch = require( '../../lib/fetch.js' );

const { convertFormattedRequestToVersionedBinary } = require( '../../executors/javascript-wasmedge/function-schemata/javascript/src/serialize.js' );
const { getError, isVoid } = require( '../../executors/javascript-wasmedge/function-schemata/javascript/src/utils.js' );
const { Evaluator } = require( '../../src/Evaluator.js' );
const { readJSON } = require( '../../src/fileUtils.js' );

async function wait( ms ) {
	await new Promise( ( resolve ) => {
		setTimeout( resolve, ms );
	} );
}

describe( 'executor pool load test', function () {

	this.timeout( 50000 );

	let uri = null;
	const server = new Server();

	before( async () => {
		await server.start();
		uri = `${ server.config.uri }wikifunctions.org/v1/evaluate/`;
	} );

	after( async () => {
		server.stop();
		// This is necessary to ensure that all spawned processes are killed;
		// without this, CI runners will wait forever because the image still
		// has active processes. If only JS had destructors ...
		await Evaluator.drainTheSwamps();
	} );

	// This test is fun to run locally but is extremely flaky in CI.
	// Un-skip it and run it if you are interested in theoretically useful code
	// that doesn't work in practice.
	it.skip( 'pool size remains consistent and executors are pre-warmed', async () => {
		const payload = readJSON( './test_data/load_test_input.json' );
		const schemaVersion = payload.schemaVersion;
		const binaryBlob = convertFormattedRequestToVersionedBinary( payload, schemaVersion );

		// For this test, we are interested in three time stamps: the time right
		// before the function is called; the time the executor actually
		// executes the function; and the time the response is received.
		const runFunctionCall = async () => {
			const startTime = new Date();
			const fetchedResult = await fetch(
				uri,
				{
					method: 'POST',
					body: binaryBlob,
					headers: { 'Content-type': 'application/octet-stream' }
				}
			);
			const endTime = new Date();
			assert.status( fetchedResult, 200 );
			const Z22 = await fetchedResult.json();
			const functionCallTime = new Date( Z22.Z22K1.Z6K1 );
			return [ startTime, functionCallTime, endTime ];
		};

		// Do not rely on the default config here. Explicitly set the executor
		// pool to a small-ish size.
		const evaluator = Evaluator.create( {
			EXECUTOR_PROCESS_POOL_SIZE: '5'
		} );
		Evaluator.setGlobalEvaluator( evaluator );

		// Wait a generous amount of time to ensure that the initial
		// executor pool is good to go (wasmedge initialization usually takes
		// ~300 ms, so let's wait longer than that).
		await wait( 2000 );

		// Now flood the zone with 20 requests.
		let promises = [ ...Array( 20 ) ].map( runFunctionCall );
		let results = await Promise.all( promises );

		const delays = [];
		for ( const [ startTime, functionCallTime ] of results ) {
			// TODO (T372765): Get more resources, then
			// destructure-assign endTime in the for loop above, then
			// re-enable the following line.
			// assert.deepEqual( endTime - functionCallTime < 100, true );
			// In all cases, endTime - functionCallTime should be negligible.
			const delay = functionCallTime - startTime;
			delays.push( delay );
		}
		delays.sort();
		const firstDelay = delays[ 0 ];

		// Ideally, we should expect these requests to run in cohorts of 5:
		// functionCallTime - startTime should be
		//   > near 0 for 5,
		//   > near 300 for 5,
		//   > near 600 for 5,
		//   > near 900 for 5.
		// The following buckets reflect these cohorts.
		// However, the tests also build in tolerance because these run times
		// are subject to a number of factors. Each bucket is therefore a
		// cumulative sum of all function calls that ran in less than a given
		// amount of time.
		const buckets = [
			0,
			0,
			0,
			0,
			0 // Stragglers.
		];
		for ( const fullDelay of delays ) {
			// Bucket based on time since the first result was retrieved.
			const delay = fullDelay - firstDelay;
			if ( delay < 250 ) {
				buckets[ 0 ] += 1;
			}
			if ( delay < 570 ) {
				buckets[ 1 ] += 1;
			}
			if ( delay < 890 ) {
				buckets[ 2 ] += 1;
			}
			if ( delay < 1210 ) {
				buckets[ 3 ] += 1;
			}
			buckets[ 4 ] += 1;
		}
		// TODO (T372765): Tighten these numbers up; buckets would ideally
		// have the values [5, 10, 15, 20, 20].
		assert.deepEqual( buckets[ 0 ] >= 3, true );
		assert.deepEqual( buckets[ 1 ] >= 6, true );
		assert.deepEqual( buckets[ 2 ] >= 9, true );
		assert.deepEqual( buckets[ 3 ] >= 12, true );
		assert.deepEqual( buckets[ 4 ], 20 );

		// Now make sure that things have settled back down.
		await wait( 2000 );

		// These 5 requests should now run very quickly.
		promises = [ ...Array( 5 ) ].map( runFunctionCall );
		results = await Promise.all( promises );
		for ( const [ startTime, functionCallTime ] of results ) {
			// TODO (T372765): Get more resources, then
			// destructure-assign endTime in the for loop above, then
			// re-enable the following lines.
			// assert.deepEqual( endTime - functionCallTime < 100, true );
			// assert.deepEqual( functionCallTime - startTime < 100, true );
			assert.deepEqual( functionCallTime - startTime < 400, true );
		}
	} );

	it( 'timeout is handled correctly after queueing for a long time', async () => {
		const payload = readJSON( './test_data/load_test_timeout_input.json' );
		const schemaVersion = payload.schemaVersion;
		const binaryBlob = convertFormattedRequestToVersionedBinary( payload, schemaVersion );

		// For this test, we are interested in three time stamps: the time right
		// before the function is called; the time the executor actually
		// executes the function; and the time the response is received.
		const runFunctionCall = async () => {
			const fetchedResult = await fetch(
				uri,
				{
					method: 'POST',
					body: binaryBlob,
					headers: { 'Content-type': 'application/octet-stream' }
				}
			);
			const Z22 = await fetchedResult.json();
			return Z22;
		};

		// Do not rely on the default config here. Explicitly set the executor
		// pool to 1.
		const evaluator = Evaluator.create( {
			EXECUTOR_PROCESS_POOL_SIZE: '5'
		} );
		Evaluator.setGlobalEvaluator( evaluator );

		// Wait a generous amount of time to ensure that the initial
		// executor pool is good to go (wasmedge initialization usually takes
		// ~300 ms, so let's wait longer than that).
		await wait( 1000 );

		// Now flood the zone with 20 requests.
		const promises = [];
		for ( let i = 0; i < 20; ++i ) {
			promises.push( runFunctionCall() );
		}
		const results = await Promise.all( promises );

		// The first 5 requests should be able to run within the default
		// 15,000-second timeout window. The rest should surface a timeout
		// error.
		let numberGood = 0;
		for ( let i = 0; i < results.length; ++i ) {
			const Z22 = results[ i ];
			const theError = getError( Z22 );
			if ( isVoid( Z22.Z22K1 ) ) {
				assert.deepEqual( theError.Z5K1.Z9K1, 'Z575' );
			} else {
				numberGood += 1;
			}
		}
		assert.deepEqual( numberGood > 2, true );
	} );

} );

const fs = require( 'fs' );

const setModuleType = ( packageJsonFileName ) => {
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	const originalContents = JSON.parse( fs.readFileSync( packageJsonFileName ) );
	originalContents.type = 'module';
	const prettyJson = JSON.stringify( originalContents, null, 4 );
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	fs.writeFileSync( packageJsonFileName, prettyJson );
};

setModuleType( process.argv[ 2 ] );

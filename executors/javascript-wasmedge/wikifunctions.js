const DEBUG_ZID_ = 'Z820';

class Wikifunctions {

	constructor( readFromStdin, writeToStdout ) {
		this.readFromStdin_ = readFromStdin;
		this.writeToStdout_ = writeToStdout;
	}

	Call( ZID ) {
		if ( ZID === DEBUG_ZID_ ) {
			return this.Z820.bind( this );
		}
	}

	Z820( key, logString ) {
		const payload = {
			key: key,
			logString: logString
		};
		this.writeToStdout_( 'start debug log <<<' );
		this.writeToStdout_( JSON.stringify( payload ) );
		this.writeToStdout_( '>>> end debug log' );
	}

	Debug( loggedString ) {
		this.Z820( 'executorDebugLogs', loggedString );
	}

}

export { Wikifunctions };

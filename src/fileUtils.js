'use strict';

const fs = require( 'fs' );

function programmingLanguageDir() {
	return process.env.PROGRAMMING_LANGUAGE_DIR || '/srv/service/programming-languages';
}

function readJSON( fileName ) {
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	return JSON.parse( fs.readFileSync( fileName, { encoding: 'utf8' } ) );
}

module.exports = {
	programmingLanguageDir,
	readJSON
};

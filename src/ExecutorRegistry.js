'use strict';

class ExecutorRegistry {
	static executorClasses = [];

	static codingLanguageToClassIndex = new Map();

	static registerExecutorConfiguration( codingLanguages, executorInterfaceClass ) {
		const index = ExecutorRegistry.executorClasses.length;
		ExecutorRegistry.executorClasses.push( executorInterfaceClass );
		for ( const codingLanguage of codingLanguages ) {
			ExecutorRegistry.codingLanguageToClassIndex.set( codingLanguage, index );
		}
	}
}

module.exports = { ExecutorRegistry };

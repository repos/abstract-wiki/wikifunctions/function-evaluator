use function_evaluator::service::get_app;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    let app = get_app();
    let listener = tokio::net::TcpListener::bind("0.0.0.0:5000").await?;
    axum::serve(listener, app).await?;
    // Go to http://localhost:5000/info; you should see the JSON response.
    // Ctrl-C to exit.
    Ok(())
}

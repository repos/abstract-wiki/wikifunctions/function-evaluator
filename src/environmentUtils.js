'use strict';

const DEFAULT_TIMEOUT_ = 15000;
const DEFAULT_WASMEDGE_GAS_ = 1 * 1000 * 1000;
const DEFAULT_WASMEDGE_MEMORY_ = 10000;
const DEFAULT_EXECUTOR_PROCESS_POOL_SIZE_ = 10;

function getNumericValueWithDefault( valueString, getDefaultNumber ) {
	let result = Number( valueString );
	if ( isNaN( result ) || result <= 0 ) {
		result = getDefaultNumber();
	}
	return result;
}

function getProcessPoolSizeForEnvironment( environment ) {
	return getNumericValueWithDefault(
		environment.EXECUTOR_PROCESS_POOL_SIZE || '',
		() => DEFAULT_EXECUTOR_PROCESS_POOL_SIZE_
	);
}

function getTimeoutForEnvironment( environment ) {
	return getNumericValueWithDefault(
		environment.FUNCTION_EVALUATOR_TIMEOUT_MS || '',
		() => DEFAULT_TIMEOUT_
	);
}

function getWasmedgeGasLimitForEnvironment( environment ) {
	return getNumericValueWithDefault(
		environment.WASMEDGE_GAS_LIMIT || '',
		() => DEFAULT_WASMEDGE_GAS_
	);
}

function getWasmedgeMemoryPageLimitForEnvironment( environment ) {
	return getNumericValueWithDefault(
		environment.WASMEDGE_MEMORY_PAGE_LIMIT || '',
		() => DEFAULT_WASMEDGE_MEMORY_
	);
}

function getWasmedgeTimeoutForEnvironment( environment ) {
	return getNumericValueWithDefault(
		environment.WASMEDGE_TIMEOUT_MS || '',
		// Default to twice the timeout for the evaluator as a whole.
		() => 2 * getTimeoutForEnvironment( environment, DEFAULT_TIMEOUT_ )
	);
}

function getTimeout() {
	return getTimeoutForEnvironment( process.env, DEFAULT_TIMEOUT_ );
}

module.exports = {
	getProcessPoolSizeForEnvironment,
	getTimeout,
	getTimeoutForEnvironment,
	getWasmedgeGasLimitForEnvironment,
	getWasmedgeMemoryPageLimitForEnvironment,
	getWasmedgeTimeoutForEnvironment
};

import io
import json
import os
import re
import unittest

from . import utils as test_utils
from .. import executor
from .. import utils as src_utils


_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data")


def _read_test_json(file_name):
    with open(os.path.join(_DATA_DIR, file_name), "r") as inp:
        return json.load(inp)


def _get_error(metadata):
    metadata_items = src_utils.convert_zlist_to_list(metadata["K1"])
    for item in metadata_items:
        if item.get("K1", {}).get("Z6K1", "") == "errors":
            return item.get("K2")


class ExecutorTest(unittest.TestCase):
    maxDiff = None

    def assert_envelopes_are_equal(self, expected_envelope, actual_pair):
        # Compare expected results.
        expected_result = expected_envelope["Z22K1"]
        actual_result = actual_pair[0]
        if actual_result is None:
            self.assertTrue(src_utils._is_zVoid(expected_result))
        else:
            self.assertEqual(
                test_utils.without_z1k1s(expected_result),
                test_utils.without_z1k1s(actual_result),
            )

        # Compare expected metadata.
        expected_metadata = expected_envelope["Z22K2"]
        actual_error = actual_pair[1]
        if actual_error is None:
            self.assertTrue(src_utils._is_zVoid(expected_metadata))
        else:
            self.assertEqual(
                test_utils.without_z1k1s(_get_error(expected_metadata)),
                test_utils.without_z1k1s(actual_error),
            )


class ExecuteTest(ExecutorTest):

    def _run_test(self, zobject, expected_envelope, expected_stdout=None):
        if expected_stdout is None:
            expected_stdout = []

        class FakeStdout:
            def __init__(self):
                self._buffer = []

            @property
            def buffer(self):
                return self._buffer

            def write(self, hopefully_string):
                self._buffer.append(hopefully_string)

            def flush(self):
                pass

        stdout = FakeStdout()
        actual = executor.execute(zobject, None, stdout)
        self.assert_envelopes_are_equal(expected_envelope, actual)
        self.assertEqual(expected_stdout, stdout.buffer)

    def test_runs_function_call(self):
        function_call = _read_test_json("python3_add.json")
        expected = _read_test_json("add_expected.json")
        self._run_test(function_call, expected)

    def test_runs_function_call_with_type_converters(self):
        function_call = _read_test_json("python3_add_with_type_converters.json")
        expected = _read_test_json("add_Z10000_expected.json")
        self._run_test(function_call, expected)

    def test_runs_function_call_with_generics(self):
        function_call = _read_test_json("python3_add_with_generics.json")
        expected = _read_test_json("add_expected.json")
        self._run_test(function_call, expected)

    def test_runs_lambda(self):
        function_call = _read_test_json("python3_add_lambda.json")
        expected = _read_test_json("add_expected.json")
        self._run_test(function_call, expected)

    def test_various_types(self):
        function_call = _read_test_json("python3_compound_type.json")
        expected = _read_test_json("compound_type_expected.json")
        self._run_test(function_call, expected)

    def test_various_types_generic(self):
        function_call = _read_test_json("python3_compound_type_generic.json")
        expected = _read_test_json("compound_type_expected.json")
        self._run_test(function_call, expected)

    def test_list_o_lists_o_strings_input(self):
        function_call = {
            "function": {
                "codeString": _read_test_json(
                    "list_list_string_input_python3_implementation.json"
                )["Z6K1"],
                "functionName": "Z1000",
            },
            "functionArguments": {
                "Z1000K1": {"object": _read_test_json("list_list_strings.json")}
            },
        }
        expected = _read_test_json("result_envelope_template.json")
        expected["Z22K1"] = _read_test_json("string_in_lists.json")
        self._run_test(function_call, expected)

    def test_list_o_lists_o_strings_output(self):
        function_call = {
            "function": {
                "codeString": _read_test_json(
                    "list_list_string_output_python3_implementation.json"
                )["Z6K1"],
                "functionName": "Z1000",
            },
            "functionArguments": {
                "Z1000K1": {"object": _read_test_json("string_in_lists.json")}
            },
        }
        expected = _read_test_json("result_envelope_template.json")
        expected["Z22K1"] = _read_test_json("list_list_strings.json")
        self._run_test(function_call, expected)

    def test_pair_string_pair_string_string_input(self):
        function_call = {
            "function": {
                "codeString": _read_test_json(
                    "pair_string_pair_string_string_input_python3_implementation.json"
                )["Z6K1"],
                "functionName": "Z1000",
            },
            "functionArguments": {
                "Z1000K1": {
                    "object": _read_test_json("pair_string_pair_string_string.json")
                }
            },
        }
        expected = _read_test_json("result_envelope_template.json")
        expected["Z22K1"] = _read_test_json("string_in_pairs.json")
        self._run_test(function_call, expected)

    def test_pair_string_pair_string_string_output(self):
        function_call = {
            "function": {
                "codeString": _read_test_json(
                    "pair_string_pair_string_string_output_python3_implementation.json"
                )["Z6K1"],
                "functionName": "Z1000",
            },
            "functionArguments": {
                "Z1000K1": {"object": _read_test_json("string_in_pairs.json")}
            },
        }
        expected = _read_test_json("result_envelope_template.json")
        expected["Z22K1"] = _read_test_json("pair_string_pair_string_string.json")
        self._run_test(function_call, expected)

    def test_map_string_string(self):
        function_call = _read_test_json("map_string_string_Z7.json")
        function_call["functionArguments"]["Z1802K1"]["object"] = _read_test_json(
            "map_string_bool.json"
        )
        function_call["function"]["codeString"] = _read_test_json(
            "map_string_string_python3_implementation.json"
        )["Z6K1"]
        expected = _read_test_json("result_envelope_template.json")
        expected["Z22K1"] = _read_test_json("map_string_string.json")
        self._run_test(function_call, expected)

    def test_user_defined_input(self):
        function_call = _read_test_json("python3_user_defined_input.json")
        function_call["functionArguments"]["Z1000K1"]["object"] = _read_test_json(
            "user_defined_input_Z1000K1.json"
        )
        expected = _read_test_json("user_defined_input_expected.json")
        self._run_test(function_call, expected)

    def test_unserializable_type(self):
        function_call = _read_test_json("python3_unsupported_output.json")
        expected = _read_test_json("python3_unsupported_output_expected.json")
        self._run_test(function_call, expected)

    def test_with_debug(self):
        function_call = _read_test_json("python3_with_debug.json")
        expected = _read_test_json("hay_Z10000_expected.json")
        expected_payload = {"key": "executorDebugLogs", "logString": "hay"}
        expected_stdout = [
            "start debug log <<<\n",
            json.dumps(expected_payload) + "\n",
            ">>> end debug log\n",
        ]
        self._run_test(function_call, expected, expected_stdout)

    def test_with_debug_Z820(self):
        function_call = _read_test_json("python3_with_debug_Z820.json")
        expected = _read_test_json("hay_Z10000_expected.json")
        expected_payload = {"key": "executorDebugLogs", "logString": "hay"}
        expected_stdout = [
            "start debug log <<<\n",
            json.dumps(expected_payload) + "\n",
            ">>> end debug log\n",
        ]
        self._run_test(function_call, expected, expected_stdout)

    def test_no_function(self):
        function_call = _read_test_json("python3_no_function.json")
        expected = _read_test_json("no_function_name_expected.json")
        self._run_test(function_call, expected)

    def test_no_function_name(self):
        function_call = _read_test_json("python3_no_function_name.json")
        expected = _read_test_json("no_function_name_expected.json")
        self._run_test(function_call, expected)

    def test_no_code_string(self):
        function_call = _read_test_json("python3_no_code_string.json")
        expected = _read_test_json("no_code_string_expected.json")
        self._run_test(function_call, expected)


class MainTest(ExecutorTest):
    def setUp(self):
        self._stdin = io.StringIO()
        self._stdout = io.StringIO()

    def _run_test(self, function_call_file, expected_file):
        # Run the function call.
        function_call_full = _read_test_json(function_call_file)
        function_call_string = json.dumps(function_call_full)
        self._stdin.write(function_call_string + "\n")
        self._stdin.seek(0)
        executor.main(self._stdin, self._stdout)
        self._stdin.close()

        # Digest all stdout.
        self._stdout.seek(0)
        lines = [line for data in self._stdout for line in data.split("\n") if line]
        print(lines)

        # Make assertions about all stdout lines except for the result.
        self.assertEqual("start internal log <<<", lines[0])
        call_start_re = r"^calling execute in executor\.\.\., original time: \d{4}"
        self.assertIsNotNone(re.search(call_start_re, lines[1]))
        self.assertEqual(">>> end internal log", lines[2])
        self.assertEqual("start internal log <<<", lines[3])
        call_end_re = (
            r"^\.\.\.finished calling execute in executor, original time: \d{4}"
        )
        self.assertIsNotNone(re.search(call_end_re, lines[4]))
        self.assertEqual(">>> end internal log", lines[5])
        self.assertEqual("start result <<<", lines[6])
        self.assertEqual(">>> end result", lines[8])

        # Parse the result and test against expected value.
        actual = json.loads(lines[7].strip())
        expected = _read_test_json(expected_file)
        self.assert_envelopes_are_equal(expected, actual)

    def test_main_add(self):
        self._run_test("python3_add.json", "add_expected.json")

    def test_main_regex(self):
        self._run_test("python3_regexes.json", "regexes_expected.json")

    def test_main_syntax_failure(self):
        self._run_test(
            "python3_syntax_failure.json", "python3_syntax_failure_expected.json"
        )

    def test_main_list_of_type_converters(self):
        self._run_test(
            "python3_list_of_type_converters.json",
            "python3_list_of_type_converters_expected.json",
        )


if __name__ == "__main__":
    unittest.main()

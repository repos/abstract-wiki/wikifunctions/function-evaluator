'use strict';

const sUtil = require( '../lib/util' );

const { rateLimitMiddleware } = require( '../middleware/rateLimitMiddleware.js' );
const { transformRequestBodyMiddleware } = require( '../middleware/avroCodec.js' );

const { Evaluator } = require( '../src/Evaluator.js' );

/**
 * The main router object
 */
const router = sUtil.router();

/**
 * The main application object reported when this module is require()d
 */
let app;

router.post( '/', [ transformRequestBodyMiddleware, rateLimitMiddleware ], async ( req, res ) => {
	app.logger.log( 'info',
		{ message: 'calling executor in evaluator...', requestId: res.requestId }
	);

	const wss = app.wss;
	const functionCallRequest = req.body;
	const result = await Evaluator.getGlobalEvaluator().evaluate( functionCallRequest, wss );

	app.logger.log( 'info',
		{ message: '...finished calling executor in evaluator', requestId: res.requestId }
	);

	if ( res.writableEnded || result === null ) {
		return;
	}
	res.json( result );

} );

module.exports = function ( appObj ) {

	app = appObj;

	return {
		path: '/evaluate',
		api_version: 1, // must be a number!
		router: router
	};

};

'use strict';

const rateLimit = require( 'express-rate-limit' );

let maxRequestsPerId;
try {
	maxRequestsPerId = parseInt( process.env.MAX_REQUESTS_PER_ID );
} finally {
	if ( isNaN( maxRequestsPerId ) ) {
		maxRequestsPerId = 100; // default to 100 requests per request ID
	}
}

let rateLimitWindowMs;
try {
	rateLimitWindowMs = parseInt( process.env.RATE_LIMIT_WINDOW_MS );
} finally {
	if ( isNaN( rateLimitWindowMs ) ) {
		rateLimitWindowMs = 10 * 60 * 1000; // default to 10 minutes
	}
}

const rateLimitMiddleware = rateLimit( {
	windowMs: rateLimitWindowMs, // 10 minutes
	max: maxRequestsPerId,
	standardHeaders: true,
	legacyHeaders: false,
	keyGenerator: ( request ) => request.body.requestId
} );

module.exports = { rateLimitMiddleware };

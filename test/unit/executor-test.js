'use strict';

const assert = require( '../utils/assert' );
const { Executor } = require( '../../src/Executor.js' );
const { readJSON } = require( '../../src/fileUtils.js' );
const { Readable, Writable } = require( 'stream' );

class TestProcess {

	constructor( fakeStdoutArray ) {
		this.stdout_ = new Readable( {
			read() {}
		} );
		for ( const fakeStdout of fakeStdoutArray ) {
			this.stdout_.push( fakeStdout );
		}
		this.stdin_ = new Writable( {
			write() {}
		} );
		Object.defineProperty( this, 'stdout', {
			get: function () {
				return this.stdout_;
			}
		} );
		Object.defineProperty( this, 'stdin', {
			get: function () {
				return this.stdin_;
			}
		} );
	}

	// Has to be implemented in order to handle on( 'close' ) calls in Executor.
	on() {}

}

class TestExecutor extends Executor {

	constructor( fakeStdoutArray ) {
		super();
		this.fakeStdout_ = fakeStdoutArray;
		this.startExecutorSubprocess();
	}

	end( resolve ) {
		resolve();
	}

	startExecutorSubprocess() {
		this.childProcess_ = new TestProcess( this.fakeStdout_ );
	}

}

class TestExecutorBadStart extends TestExecutor {

	getStartAction( data ) {
		const result = super.getStartAction( data );
		if ( result === null ) {
			return 'ꙮ';
		}
	}

}

const FakeParseState = Object.freeze( {
	NOTHING: Symbol( 'FakeParseState.NOTHING' )
} );

describe( 'src-executor-test', function () {

	this.timeout( 10000 );

	it( 'reentrant call with non-reentrant evaluator', async () => {
		const expectedResult = readJSON( './test_data/non-reentrant-evaluator_expected.json' );
		const testExecutor = new TestExecutor( [
			'start reentrant call <<<',
			'>>> end reentrant call'
		] );
		const result = await testExecutor.runExecution(
			/* functionCallRequest= */ {},
			/* websocket= */ null
		);
		assert.deepEqual( result, expectedResult );
	} );

	it( 'no start state', async () => {
		const expectedResult = readJSON( './test_data/no-start-state_expected.json' );
		const testExecutor = new TestExecutor( [
			'start reentrant call <<<'
		] );
		testExecutor.currentStateAndQueue_ = [ undefined, [] ];
		const result = await testExecutor.runExecution(
			/* functionCallRequest= */ {},
			/* websocket= */ null
		);
		assert.deepEqual( result, expectedResult );
	} );

	it( 'processStartAction with weird action', async () => {
		const expectedResult = readJSON( './test_data/weird-start-action_expected.json' );
		const testExecutor = new TestExecutorBadStart( [
			'start nothing good <<<'
		] );
		const result = await testExecutor.runExecution(
			/* functionCallRequest= */ {},
			/* websocket= */ null
		);
		assert.deepEqual( result, expectedResult );
	} );

	it( 'start action interrupts in-process action', async () => {
		const expectedResult = readJSON( './test_data/interrupted-start-action_expected.json' );
		const testExecutor = new TestExecutor( [] );
		testExecutor.currentStateAndQueue_ = [ FakeParseState.NOTHING, [] ];
		const result = testExecutor.processStartAction( FakeParseState.NOTHING );
		assert.deepEqual( result, expectedResult );
	} );

} );

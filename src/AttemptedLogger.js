'use strict';

class AttemptedLogger {

	constructor( logger ) {
		this.logger_ = logger;
	}

	log( ...args ) {
		if ( this.logger_ ) {
			return this.logger_.log( ...args );
		}
	}

}

module.exports = { AttemptedLogger };

import * as fs from 'fs';
import { execute, runFunction } from '../executor.js';
import { withoutZ1K1s } from './utils.js';
import { assert } from 'chai';
import { isVoid } from '../function-schemata/javascript/shared/es6UtilsModule.js';

function readTestJson( fileName ) {
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	return JSON.parse( fs.readFileSync( './test/test_data/' + fileName, { encoding: 'utf8' } ) );
}

function writeStdoutUnbound( stdoutQueue, contents ) {
	stdoutQueue.push( contents );
}

describe( 'JavaScript executor: runFunction', () => {

	let stdoutQueue;
	let writeStdout;

	beforeEach( () => {
		stdoutQueue = [];
		writeStdout = writeStdoutUnbound.bind( null, stdoutQueue );
	} );

	async function testRunFunction( functionCall, expectedEnvelope ) {
		const functionCallString = JSON.stringify( functionCall );
		function readStdin() {
			return functionCallString;
		}
		await runFunction( readStdin, writeStdout );

		assert.deepEqual( 'start internal log <<<', stdoutQueue[ 0 ] );
		assert.isOk( stdoutQueue[ 1 ].match( /^calling execute in executor\.\.\., original time: / ) );
		assert.deepEqual( '>>> end internal log', stdoutQueue[ 2 ] );
		assert.deepEqual( 'start internal log <<<', stdoutQueue[ 3 ] );
		assert.isOk( stdoutQueue[ 4 ].match( /^\.\.\.finished calling execute in executor, original time: / ) );
		assert.deepEqual( '>>> end internal log', stdoutQueue[ 5 ] );
		assert.deepEqual( 'start result <<<', stdoutQueue[ 6 ] );
		assert.deepEqual( '>>> end result', stdoutQueue[ 8 ] );
		const actualResult = stdoutQueue[ 7 ];
		const result = JSON.parse( actualResult );

		if ( isVoid( expectedEnvelope.Z22K2 ) ) {
			assert.isNull( result[ 1 ] );
			assert.deepEqual( withoutZ1K1s( expectedEnvelope.Z22K1 ), withoutZ1K1s( result[ 0 ] ) );
		} else {
			// FIXME: Also check result[ 1 ] (errors)
			assert.isNull( result[ 0 ] );
			assert.deepEqual(
				withoutZ1K1s( expectedEnvelope.Z22K2.K1.K1.K2 ),
				withoutZ1K1s( result[ 1 ] ) );
		}
	}

	it( 'test runFunction: add', async () => {
		const functionCall = readTestJson( 'javascript_add.json' );
		const expectedEnvelope = readTestJson( 'add_expected.json' );
		await testRunFunction( functionCall, expectedEnvelope );
	} );

	it( 'test runFunction: regexes', async () => {
		const functionCall = readTestJson( 'javascript_regexes.json' );
		const expectedEnvelope = readTestJson( 'regexes_expected.json' );
		await testRunFunction( functionCall, expectedEnvelope );
	} );

	it( 'test runFunction: syntax failure', async () => {
		const functionCall = readTestJson( 'javascript_syntax_failure.json' );
		const expectedEnvelope = readTestJson( 'javascript_syntax_failure_expected.json' );
		await testRunFunction( functionCall, expectedEnvelope );
	} );

	async function testExecute( zobject, expectedResult, expectedStdout = [] ) {
		// FIXME: Also check result[ 1 ] (errors)
		const result = await execute( zobject, null, writeStdout );
		if ( isVoid( expectedResult.Z22K1 ) ) {
			assert.isNull( result[ 0 ] );
		} else {
			assert.deepEqual( withoutZ1K1s( expectedResult.Z22K1 ), withoutZ1K1s( result[ 0 ] ) );
		}
		assert.deepEqual( expectedStdout, stdoutQueue );
	}

	it( 'test runs function call', async () => {
		await testExecute(
			readTestJson( 'javascript_add.json' ),
			readTestJson( 'add_expected.json' )
		);
	} );

	it( 'test runs function call with generics', async () => {
		await testExecute(
			readTestJson( 'javascript_add_with_generics.json' ),
			readTestJson( 'add_expected.json' )
		);
	} );

	it( 'test compound type', async () => {
		await testExecute(
			readTestJson( 'javascript_compound_type.json' ),
			readTestJson( 'compound_type_expected.json' )
		);
	} );

	it( 'test list_o_lists_o_strings_input', async () => {
		const functionCall = {
			function: {
				functionName: 'Z1000',
				codeString: readTestJson( 'list_list_string_input_javascript_implementation.json' ).Z6K1
			},
			functionArguments: {
				Z1000K1: {
					object: readTestJson( 'list_list_strings.json' )
				}
			}
		};
		const expected = readTestJson( 'result_envelope_template.json' );
		expected.Z22K1 = readTestJson( 'string_in_lists.json' );
		await testExecute( functionCall, expected );
	} );

	it( 'test list_o_lists_o_strings_output', async () => {
		const functionCall = {
			function: {
				functionName: 'Z1000',
				codeString: readTestJson( 'list_list_string_output_javascript_implementation.json' ).Z6K1
			},
			functionArguments: {
				Z1000K1: {
					object: readTestJson( 'string_in_lists.json' )
				}
			}
		};
		const expected = readTestJson( 'result_envelope_template.json' );
		expected.Z22K1 = readTestJson( 'list_list_strings.json' );
		await testExecute( functionCall, expected );
	} );

	it( 'test list_o_lists_o_strings_output_unspecified', async () => {
		const functionCall = {
			function: {
				functionName: 'Z1000',
				codeString: readTestJson( 'list_list_string_output_javascript_implementation.json' ).Z6K1
			},
			functionArguments: {
				Z1000K1: {
					object: readTestJson( 'string_in_lists.json' )
				}
			}
		};
		const expected = readTestJson( 'result_envelope_template.json' );
		expected.Z22K1 = readTestJson( 'list_list_strings.json' );
		await testExecute( functionCall, expected );
	} );

	it( 'test pair_string_pair_string_string_input', async () => {
		const functionCall = {
			function: {
				functionName: 'Z1000',
				codeString: readTestJson( 'pair_string_pair_string_string_input_javascript_implementation.json' ).Z6K1
			},
			functionArguments: {
				Z1000K1: {
					object: readTestJson( 'pair_string_pair_string_string.json' )
				}
			}
		};
		const expected = readTestJson( 'result_envelope_template.json' );
		expected.Z22K1 = readTestJson( 'string_in_pairs.json' );
		await testExecute( functionCall, expected );
	} );

	it( 'test pair_string_pair_string_string_output', async () => {
		const functionCall = {
			function: {
				functionName: 'Z1000',
				codeString: readTestJson( 'pair_string_pair_string_string_output_javascript_implementation.json' ).Z6K1
			},
			functionArguments: {
				Z1000K1: {
					object: readTestJson( 'string_in_pairs.json' )
				}
			}
		};
		const expected = readTestJson( 'result_envelope_template.json' );
		expected.Z22K1 = readTestJson( 'pair_string_pair_string_string.json' );
		await testExecute( functionCall, expected );
	} );

	it( 'test map_string_string', async () => {
		const functionCall = readTestJson( 'map_string_string_Z7.json' );
		functionCall.function.codeString = readTestJson( 'map_string_string_javascript_implementation.json' ).Z6K1;
		functionCall.functionArguments.Z1802K1.object = readTestJson( 'map_string_bool.json' );
		const expected = readTestJson( 'result_envelope_template.json' );
		expected.Z22K1 = readTestJson( 'map_string_string.json' );
		await testExecute( functionCall, expected );
	} );

	it( 'test user-defined input', async () => {
		const functionCall = readTestJson( 'javascript_user_defined_input.json' );
		functionCall.functionArguments.Z1000K1.object = readTestJson( 'user_defined_input_Z1000K1.json' );
		await testExecute( functionCall, readTestJson( 'user_defined_input_expected.json' ) );
	} );

	it( 'test unserializable type', async () => {
		await testExecute(
			readTestJson( 'javascript_unsupported_output.json' ),
			readTestJson( 'javascript_unsupported_output_expected.json' )
		);
	} );

	it( 'test with type converters', async () => {
		await testExecute(
			readTestJson( 'javascript_add_with_type_converters.json' ),
			readTestJson( 'add_Z10000_expected.json' )
		);
	} );

	it( 'test with debug', async () => {
		const expectedPayload = {
			key: 'executorDebugLogs',
			logString: 'hay'
		};
		await testExecute(
			readTestJson( 'javascript_add_with_debug.json' ),
			readTestJson( 'hay_Z10000_expected.json' ),
			[
				'start debug log <<<',
				JSON.stringify( expectedPayload ),
				'>>> end debug log' ]
		);
	} );

	it( 'test with debug Z820', async () => {
		const expectedPayload = {
			key: 'executorDebugLogs',
			logString: 'hay'
		};
		await testExecute(
			readTestJson( 'javascript_Z820_debug.json' ),
			readTestJson( 'hay_Z10000_expected.json' ),
			[
				'start debug log <<<',
				JSON.stringify( expectedPayload ),
				'>>> end debug log' ]
		);
	} );

	it( 'test no function', async () => {
		await testExecute(
			readTestJson( 'javascript_no_function.json' ),
			readTestJson( 'no_function_expected.json' )
		);
	} );

	it( 'test no functionName', async () => {
		await testExecute(
			readTestJson( 'javascript_no_function_name.json' ),
			readTestJson( 'no_function_name_expected.json' )
		);
	} );

	it( 'test no codeString', async () => {
		await testExecute(
			readTestJson( 'javascript_no_code_string.json' ),
			readTestJson( 'no_code_string_expected.json' )
		);
	} );

	it( 'test list of type converters', async () => {
		await testExecute(
			readTestJson( 'javascript_list_of_type_converters.json' ),
			readTestJson( 'javascript_list_of_type_converters_expected.json' )
		);
	} );

} );

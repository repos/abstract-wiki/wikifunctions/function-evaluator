'use strict';

const { readJSON } = require( '../../src/fileUtils.js' );

const javascriptDontChoke = {
	Z1K1: {
		Z1K1: 'Z9',
		Z9K1: 'Z7'
	},
	Z7K1: {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z8'
		},
		Z8K1: {
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z881'
				},
				Z881K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z17'
				}
			},
			K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z17'
				},
				Z17K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z6'
				},
				Z17K2: {
					Z1K1: 'Z6',
					Z6K1: 'Z1000K1'
				},
				Z17K3: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z12'
					},
					Z12K1: {
						Z1K1: {
							Z1K1: {
								Z1K1: 'Z9',
								Z9K1: 'Z7'
							},
							Z7K1: {
								Z1K1: 'Z9',
								Z9K1: 'Z881'
							},
							Z881K1: {
								Z1K1: 'Z9',
								Z9K1: 'Z11'
							}
						}
					}
				}
			},
			K2: {
				Z1K1: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z7'
					},
					Z7K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z881'
					},
					Z881K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z17'
					}
				}
			}
		},
		Z8K2: {
			Z1K1: 'Z9',
			Z9K1: 'Z6'
		},
		Z8K3: {
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z881'
				},
				Z881K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z20'
				}
			}
		},
		Z8K4: {
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z881'
				},
				Z881K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z14'
				}
			},
			K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z14'
				},
				Z14K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z1000'
				},
				Z14K3: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z16'
					},
					Z16K1: {
						Z1K1: {
							Z1K1: 'Z9',
							Z9K1: 'Z61'
						},
						Z61K1: {
							Z1K1: 'Z6',
							Z6K1: 'javascript'
						}
					},
					Z16K2: {
						Z1K1: 'Z6',
						Z6K1: 'function Z1000( Z1000K1 ) {\treturn Z1000K1; }'
					}
				}
			},
			K2: {
				Z1K1: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z7'
					},
					Z7K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z881'
					},
					Z881K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z14'
					}
				}
			}
		},
		Z8K5: {
			Z1K1: 'Z9',
			Z9K1: 'Z1000'
		}
	},
	Z1000K1: {
		Z1K1: 'Z6',
		Z6K1: 'foo'
	}
};

const pythonDontChoke = {
	Z1K1: {
		Z1K1: 'Z9',
		Z9K1: 'Z7'
	},
	Z7K1: {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z8'
		},
		Z8K1: {
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z881'
				},
				Z881K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z17'
				}
			},
			K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z17'
				},
				Z17K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z6'
				},
				Z17K2: {
					Z1K1: 'Z6',
					Z6K1: 'Z1000K1'
				},
				Z17K3: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z12'
					},
					Z12K1: {
						Z1K1: {
							Z1K1: {
								Z1K1: 'Z9',
								Z9K1: 'Z7'
							},
							Z7K1: {
								Z1K1: 'Z9',
								Z9K1: 'Z881'
							},
							Z881K1: {
								Z1K1: 'Z9',
								Z9K1: 'Z11'
							}
						}
					}
				}
			},
			K2: {
				Z1K1: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z7'
					},
					Z7K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z881'
					},
					Z881K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z17'
					}
				}
			}
		},
		Z8K2: {
			Z1K1: 'Z9',
			Z9K1: 'Z6'
		},
		Z8K3: {
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z881'
				},
				Z881K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z20'
				}
			}
		},
		Z8K4: {
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z881'
				},
				Z881K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z14'
				}
			},
			K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z14'
				},
				Z14K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z1000'
				},
				Z14K3: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z16'
					},
					Z16K1: {
						Z1K1: {
							Z1K1: 'Z9',
							Z9K1: 'Z61'
						},
						Z61K1: {
							Z1K1: 'Z6',
							Z6K1: 'python-3'
						}
					},
					Z16K2: {
						Z1K1: 'Z6',
						Z6K1: 'def Z1000(Z1000K1):\n\treturn Z1000K1'
					}
				}
			},
			K2: {
				Z1K1: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z7'
					},
					Z7K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z881'
					},
					Z881K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z14'
					}
				}
			}
		},
		Z8K5: {
			Z1K1: 'Z9',
			Z9K1: 'Z1000'
		}
	},
	Z1000K1: {
		Z1K1: 'Z6',
		Z6K1: 'foo'
	}
};

const javascriptTestData = [
	{
		name: 'javascript - addition',
		input: readJSON( './test_data/javascript_add.json' ),
		expectedOutput: readJSON( './test_data/add_expected.json' ),
		useBinaryFormatVersion: '0.1.0'
	},
	{
		name: 'javascript - addition (with generics)',
		input: readJSON( './test_data/javascript_add_with_generics_Z7.json' ),
		expectedOutput: readJSON( './test_data/add_expected.json' )
	},
	{
		name: 'javascript - can handle escape sequences',
		input: readJSON( './test_data/javascript_regexes.json' ),
		expectedOutput: readJSON( './test_data/regexes_expected.json' ),
		useBinaryFormatVersion: '0.1.1'
	},
	{
		name: 'javascript - cumulative sum (with lists of convertible types)',
		input: readJSON( './test_data/javascript_list_of_type_converters.json' ),
		expectedOutput: readJSON( './test_data/javascript_list_of_type_converters_expected.json' ),
		useBinaryFormatVersion: '0.1.1'
	},
	{
		name: 'javascript - newline in the output',
		input: readJSON( './test_data/javascript_newline_Z7.json' ),
		expectedOutput: readJSON( './test_data/newline_expected.json' )
	},
	{
		name: 'javascript - return map',
		input: readJSON( './test_data/javascript_return_map.json' ),
		expectedOutput: readJSON( './test_data/return_map_expected.json' )
	},
	{
		name: 'javascript - don\'t choke on tabs',
		input: javascriptDontChoke,
		expectedOutput: readJSON( './test_data/didnt_choke.json' )
	},
	{
		// This test is intended to stress-test the code executor and ensure that
		// long-running, moderately memory-intensive algorithms can run reliably.
		name: 'javascript - inefficient Fibonacci',
		input: readJSON( './test_data/javascript_inefficient_fib_Z7.json' ),
		expectedOutput: readJSON( './test_data/inefficient_fib_expected.json' )
	},
	{
		// This test is intended to assess timeouts.
		name: 'javascript - timeout',
		input: readJSON( './test_data/javascript_timeout_Z7.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z575',
		expectedErrorKeyPhrase: ' ms',
		skipBenchmark: true
	},
	{
		name: 'javascript - throw',
		input: readJSON( './test_data/javascript_throw.json' ),
		expectedOutput: null,
		expectedErrorKeyPhrase: 'Hello, this is a good day to die'
	},
	{
		name: 'javascript - throw raw string',
		input: readJSON( './test_data/javascript_throw_raw_string.json' ),
		expectedOutput: null,
		expectedErrorKeyPhrase: 'Hello, this is a good day to die'
	},
	{
		name: 'unsupported language Java - throw',
		input: readJSON( './test_data/unsupported_language_java_throw.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z558',
		expectedErrorKeyPhrase: 'Java'
	},
	{
		name: 'javascript - debug',
		input: readJSON( './test_data/javascript_debug_Z7.json' ),
		expectedOutput: readJSON( './test_data/debug_expected.json' ),
		expectedErrorType: null,
		expectedErrorKeyPhrase: '',
		expectedExtraMetadata: [ 'executorDebugLogs' ]
	},
	{
		name: 'javascript - no whitespace test',
		input: readJSON( './test_data/javascript_exec_whitespace.json' ),
		expectedOutput: readJSON( './test_data/javascript_exec_whitespace_expected_return.json' ),
		expectedErrorType: null
	},
	{
		name: 'javascript - no whitespace with whitespace test',
		input: readJSON( './test_data/javascript_exec_with_whitespace.json' ),
		expectedOutput: readJSON( './test_data/javascript_exec_with_whitespace_expected_return.json' ),
		expectedErrorType: null
	}
];

// TODO (T372721): It is currently important that these limits not be so low
// as to cause the child process to be killed before the function can execute.
// Those cases should be tested with much lower wasmedge limits.
const javascriptWasmLimitsTestData = [
	/*
     * TODO (T382624): Re-enable this test and figure out why it runs twice.
	{
		name: 'javascript - enabled exceeds wasmedge time limit',
		input: readJSON( './test_data/javascript_timeout.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z564',
		environment: {
			ENABLE_WASMEDGE_LIMITS: '1',
			WASMEDGE_TIMEOUT_MS: '100'
		}
	},
    */
	/*
     * TODO (T372756): Re-enable this test.
	{
		name: 'javascript - enabled exceeds wasmedge gas limit',
		input: readJSON( './test_data/javascript_out_of_gas.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z564',
		expectedErrorKeyPhrase: 'Cost exceeded limit',
		environment: {
			ENABLE_WASMEDGE_LIMITS: '1',
			WASMEDGE_GAS_LIMIT: '5000'
		}
	},
    */
	{
		name: 'javascript - enabled exceeds wasmedge memory page limit',
		input: readJSON( './test_data/javascript_much_memory_wow.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z564',
		expectedErrorKeyPhrase: 'Memory grow page failed, exceeded limited 30 page size',
		environment: {
			ENABLE_WASMEDGE_LIMITS: '1',
			WASMEDGE_MEMORY_PAGE_LIMIT: '30'
		}
	},
	{
		name: 'javascript - disabled exceeds wasmedge time limit',
		input: readJSON( './test_data/javascript_timeout.json' ),
		expectedOutput: readJSON( './test_data/fibonacci_20_expected.json' ),
		environment: {
			WASMEDGE_TIMEOUT_MS: '100'
		}
	},
	{
		name: 'javascript - disabled exceeds wasmedge gas limit',
		input: readJSON( './test_data/javascript_out_of_gas.json' ),
		expectedOutput: readJSON( './test_data/fibonacci_30_expected.json' ),
		environment: {
			WASMEDGE_GAS_LIMIT: '5000'
		}
	},
	{
		name: 'javascript - disabled exceeds wasmedge memory page limit',
		input: readJSON( './test_data/javascript_much_memory_wow.json' ),
		expectedOutput: readJSON( './test_data/fibonacci_30_expected.json' ),
		environment: {
			WASMEDGE_MEMORY_PAGE_LIMIT: '30'
		}
	}
];

const pythonTestData = [
	{
		name: 'python - addition',
		input: readJSON( './test_data/python3_add.json' ),
		expectedOutput: readJSON( './test_data/add_expected.json' ),
		useBinaryFormatVersion: '0.1.0'
	},
	{
		name: 'python - addition (with generics)',
		input: readJSON( './test_data/python3_add_with_generics_Z7.json' ),
		expectedOutput: readJSON( './test_data/add_expected.json' )
	},
	{
		name: 'python - can handle escape sequences',
		input: readJSON( './test_data/python3_regexes.json' ),
		expectedOutput: readJSON( './test_data/regexes_expected.json' ),
		useBinaryFormatVersion: '0.1.1'
	},
	{
		name: 'python - cumulative sum (with lists of convertible types)',
		input: readJSON( './test_data/python3_list_of_type_converters.json' ),
		expectedOutput: readJSON( './test_data/python3_list_of_type_converters_expected.json' ),
		useBinaryFormatVersion: '0.1.1'
	},
	{
		name: 'python unsupported version - throw',
		input: readJSON( './test_data/python_unsupported_version_throw.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z558',
		expectedErrorKeyPhrase: 'python-1'
	},
	{
		// This test is intended to stress-test the code executor and ensure that
		// long-running, moderately memory-intensive algorithms can run reliably.
		name: 'python - inefficient Fibonacci',
		input: readJSON( './test_data/python3_inefficient_fib_Z7.json' ),
		expectedOutput: readJSON( './test_data/inefficient_fib_expected.json' ),
		skipBenchmark: true
	},
	{
		// This test is intended to assess timeouts.
		name: 'python - timeout',
		input: readJSON( './test_data/python3_timeout_Z7.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z575',
		expectedErrorKeyPhrase: ' ms',
		skipBenchmark: true
	},
	{
		name: 'python - don\'t choke on tabs',
		input: pythonDontChoke,
		expectedOutput: readJSON( './test_data/didnt_choke.json' )
	},
	{
		name: 'python - debug',
		input: readJSON( './test_data/python3_debug_Z7.json' ),
		expectedOutput: readJSON( './test_data/debug_expected.json' ),
		expectedErrorType: null,
		expectedErrorKeyPhrase: '',
		expectedExtraMetadata: [ 'executorDebugLogs' ]
	},
	{
		name: 'python - newline in the output',
		input: readJSON( './test_data/python3_newline_Z7.json' ),
		expectedOutput: readJSON( './test_data/newline_expected.json' )
	},
	{
		name: 'python - return map',
		input: readJSON( './test_data/python_return_map.json' ),
		expectedOutput: readJSON( './test_data/return_map_expected.json' )
	},
	{
		name: 'unsupported language Java - throw',
		input: readJSON( './test_data/unsupported_language_java_throw.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z558',
		expectedErrorKeyPhrase: 'Java'
	}
];

const pythonWasmTestData = [
	{
		name: 'python - disallow subprocess',
		input: readJSON( './test_data/python3_bad_subprocess_Z7.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		// TODO (T363703): This key phrase keeps changing.
		expectedErrorKeyPhrase: '[Errno 58] wasi does not support proc'
	},
	{
		name: 'python - disallow file reads',
		input: readJSON( './test_data/python3_bad_file_read_Z7.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: '[Errno 44] No such file or directory'
	},
	{
		name: 'python - disallow HTTP',
		input: readJSON( './test_data/python3_bad_http_Z7.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: 'module \'http\' has no attribute \'client\''
	},
	{
		name: 'python - disallow sockets',
		input: readJSON( './test_data/python3_bad_socket_Z7.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: 'No module named \'_socket\''
	},
	{
		name: 'python do not exec - throw',
		input: readJSON( './test_data/python_undefined_exec_throw.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: 'name \'result\' is not defined'
	},
	{
		name: 'python do not exec trickily, either - throw',
		input: readJSON( './test_data/python_undefined_exec_tricky_throw.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: '\'dict\' object has no attribute \'exec\''
	},
	{
		name: 'python syntax error',
		input: readJSON( './test_data/python3_invalid_syntax.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: 'invalid syntax. Got unexpected token'
	},
	{
		name: 'python value error in converter to code',
		input: readJSON( './test_data/python3_value_error_in_type_converter_to_code.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: "invalid literal for int() with base 10: ''",
		useBinaryFormatVersion: '0.1.0'
	},
	{
		name: 'python value error in converter from code',
		input: readJSON( './test_data/python3_value_error_in_type_converter_from_code.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: "invalid literal for int() with base 10: ''",
		useBinaryFormatVersion: '0.1.0'
	},
	{
		name: 'python semantic error',
		input: readJSON( './test_data/python3_invalid_semantics.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: 'name \'result\' is not defined'
	},
	{
		name: 'python - no whitespace test',
		input: readJSON( './test_data/python3_exec_whitespace.json' ),
		expectedOutput: readJSON( './test_data/python3_exec_whitespace_expected_return.json' ),
		expectedErrorType: null
	},
	{
		name: 'python - no whitespace with whitespace test',
		input: readJSON( './test_data/python3_exec_with_whitespace.json' ),
		expectedOutput: readJSON( './test_data/python3_exec_with_whitespace_expected_return.json' ),
		expectedErrorType: null
	}
];

// TODO (T372721): It is currently important that these limits not be so low
// as to cause the child process to be killed before the function can execute.
// Those cases should be tested with much lower wasmedge limits.
const pythonWasmLimitsTestData = [
	{
		name: 'python - enabled exceeds wasmedge time limit',
		input: readJSON( './test_data/python3_timeout.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z564',
		environment: {
			ENABLE_WASMEDGE_LIMITS: '1',
			WASMEDGE_TIMEOUT_MS: '1000',
			FUNCTION_EVALUATOR_TIMEOUT_MS: '60000'
		}
	},
	/*
     * TODO (T372756): Re-enable this test.
	{
		name: 'python - enabled exceeds wasmedge gas limit',
		input: readJSON( './test_data/python3_out_of_gas.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z564',
		expectedErrorKeyPhrase: 'Cost exceeded limit',
		environment: {
			ENABLE_WASMEDGE_LIMITS: '1',
			WASMEDGE_GAS_LIMIT: '1000'
		}
	},
     */
	{
		name: 'python - enabled exceeds wasmedge memory page limit',
		input: readJSON( './test_data/python3_much_memory_wow.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z564',
		expectedErrorKeyPhrase: 'Memory Instance: Limited 280 page',
		environment: {
			ENABLE_WASMEDGE_LIMITS: '1',
			WASMEDGE_MEMORY_PAGE_LIMIT: '280'
		}
	},
	{
		name: 'python - disabled exceeds wasmedge time limit',
		input: readJSON( './test_data/python3_timeout.json' ),
		expectedOutput: readJSON( './test_data/fibonacci_20_expected.json' ),
		environment: {
			WASMEDGE_TIMEOUT_MS: '1000'
		}
	},
	{
		name: 'python - disabled exceeds wasmedge gas limit',
		input: readJSON( './test_data/python3_out_of_gas.json' ),
		expectedOutput: readJSON( './test_data/fibonacci_20_expected.json' ),
		environment: {
			WASMEDGE_GAS_LIMIT: '1000'
		}
	},
	{
		name: 'python - disabled exceeds wasmedge memory page limit',
		input: readJSON( './test_data/python3_much_memory_wow.json' ),
		expectedOutput: readJSON( './test_data/fibonacci_20_expected.json' ),
		environment: {
			WASMEDGE_MEMORY_PAGE_LIMIT: '10'
		}
	}
];

const pythonWasmSkipTestData = [
	{
		name: 'python - disallow file writes',
		input: readJSON( './test_data/python3_bad_file_write_Z7.json' ),
		expectedOutput: null,
		expectedErrorType: 'Z507',
		expectedErrorKeyPhrase: 'execution failed: unreachable, Code: 0x89'
	}
];

const pythonExecutorInterfaceTestData = [
	{
		name: 'logs on degenerate debug log',
		input: readJSON( './test_data/python3_degenerate_debug.json' ),
		expectedOutput: readJSON( './test_data/none_expected.json' ),
		expectedLogs: [ [ 'warn', { message: 'Could not parse debug log: this is not JSON uwu' } ] ]
	},
	{
		name: 'logs on bad payload debug log',
		input: readJSON( './test_data/python3_bad_payload_debug_log.json' ),
		expectedOutput: readJSON( './test_data/none_expected.json' ),
		expectedLogs: [ [ 'info', { message: 'Log did not contain expected keys {key, logString}: {"am I a bad payload?": "yes"}' } ] ]
	}
];

const javascriptExecutorInterfaceTestData = [
	{
		name: 'stringifies non-string degenerate debug log',
		input: readJSON( './test_data/javascript_degenerate_debug.json' ),
		expectedOutput: readJSON( './test_data/none_expected.json' ),
		expectedExtraMetadata: [ 'executorDebugLogs' ]
	}
];

module.exports = {
	javascriptExecutorInterfaceTestData,
	javascriptTestData,
	javascriptWasmLimitsTestData,
	pythonExecutorInterfaceTestData,
	pythonTestData,
	pythonWasmTestData,
	pythonWasmLimitsTestData,
	pythonWasmSkipTestData
};

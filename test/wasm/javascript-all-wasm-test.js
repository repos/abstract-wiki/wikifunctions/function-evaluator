'use strict';

const fetch = require( '../../lib/fetch.js' );
const Server = require( '../utils/server.js' );
const { javascriptWasmLimitsTestData } = require( '../utils/data' );
const { evaluatorIntegrationTest, executorClassTest } = require( '../utils/integrationTest.js' );
const { Evaluator } = require( '../../src/Evaluator.js' );
const { readJSON } = require( '../../src/fileUtils.js' );

describe( 'javascript-wasm-v1-integration', function () {

	this.timeout( 20000 );

	let uri = null;
	const server = new Server();

	const testRunner = function ( testName, callBack ) {
		it( testName, callBack );
	};

	before( async () => {
		await server.start();
		uri = `${ server.config.uri }wikifunctions.org/v1/evaluate/`;
	} );

	after( async () => {
		server.stop();
		// This is necessary to ensure that all spawned processes are killed;
		// without this, CI runners will wait forever because the image still
		// has active processes. If only JS had destructors ...
		await Evaluator.drainTheSwamps();
	} );

	for ( const testData of javascriptWasmLimitsTestData ) {
		executorClassTest(
			testData.environment,
			testRunner,
			testData.name,
			testData.input,
			testData.expectedOutput,
			testData.expectedErrorType,
			testData.expectedErrorKeyPhrase,
			testData.expectedExtraMetadata,
			testData.expectedMissingMetadata
		);
	}

	evaluatorIntegrationTest(
		async ( requestBody ) => await fetch( uri, requestBody ),
		( testName, callBack ) => {
			it( testName, callBack );
		},
		'javascript - can handle non-ASCII characters',
		readJSON( './test_data/javascript_non_ascii_input_Z7.json' ),
		readJSON( './test_data/javascript_non_ascii_input_expected.json' )
	);

	evaluatorIntegrationTest(
		async ( requestBody ) => await fetch( uri, requestBody ),
		( testName, callBack ) => {
			it( testName, callBack );
		},
		'javascript - returns uninitalized result error',
		readJSON( './test_data/javascript_uninitialized_error.json' ),
		readJSON( './test_data/javascript_uninitialized_error_expected.json' )
	);
} );

'use strict';

const fetch = require( '../../../lib/fetch.js' );
const Server = require( '../../utils/server.js' );
const { javascriptTestData } = require( '../../utils/data' );
const { evaluatorIntegrationTest } = require( '../../utils/integrationTest.js' );
const { Evaluator } = require( '../../../src/Evaluator.js' );
const { readJSON } = require( '../../../src/fileUtils.js' );
const { convertFormattedRequestToVersionedBinary } = require( '../../../executors/javascript-wasmedge/function-schemata/javascript/src/serialize' );
const assert = require( '../../utils/assert' );

describe( 'javascript-v1-integration', function () {

	this.timeout( 20000 );

	let uri = null;
	const maxRequestsPerId = 5;
	process.env.MAX_REQUESTS_PER_ID = maxRequestsPerId.toString();
	const server = new Server();
	const fetchResult = async function ( requestBody ) {
		return await fetch( uri, requestBody );
	};
	const testRunner = function ( testName, callBack ) {
		it( testName, callBack );
	};

	before( async () => {
		await server.start();
		uri = `${ server.config.uri }wikifunctions.org/v1/evaluate/`;
	} );

	after( async () => {
		server.stop();
		// This is necessary to ensure that all spawned processes are killed;
		// without this, CI runners will wait forever because the image still
		// has active processes. If only JS had destructors ...
		await Evaluator.drainTheSwamps();
	} );

	for ( const testData of javascriptTestData ) {
		evaluatorIntegrationTest(
			fetchResult,
			testRunner,
			testData.name,
			testData.input,
			testData.expectedOutput,
			testData.expectedErrorType,
			testData.expectedErrorKeyPhrase,
			testData.expectedExtraMetadata,
			testData.expectedMissingMetadata,
			testData.useBinaryFormatVersion
		);
	}

	it( 'rate limiting', async () => {
		const functionCall = readJSON( './test_data/javascript_add.json' );
		functionCall.requestId = 'very-special-dos-attempt';
		const wrappedInput = convertFormattedRequestToVersionedBinary(
			functionCall, /* version= */ '0.1.1' );

		let numberRateLimited = 0;
		const fetchedResults = [];
		for ( let i = 0; i <= maxRequestsPerId; ++i ) {
			fetchedResults.push(
				fetch(
					uri,
					{
						method: 'POST',
						body: wrappedInput,
						headers: { 'Content-type': 'application/octet-stream' }
					} ) );
		}
		const results = await Promise.all( fetchedResults );

		for ( const fetchedResult of results ) {
			if ( fetchedResult.status === 200 ) {
				assert.status( fetchedResult, 200 );
			} else {
				// This is the one that gets rate-limited.
				assert.status( fetchedResult, 429 );
				numberRateLimited += 1;
			}
		}
		assert.deepEqual( numberRateLimited > 0, true, 'No calls were rate-limited >:(' );
	} );

} );

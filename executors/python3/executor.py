import datetime
import functools
import json
import logging
import sys

from . import serialization
from . import ztypes
from . import wikifunctions


_RUN_CODE_TEMPLATE = """
{code_string}
if _DO_SPLAT:
    result = {function_name}(**_ARGUMENTS)
else:
    result = {function_name}(_ARGUMENTS)
_RESULT_CACHE['result'] = result
"""


def _run_code_object(code_object, arguments, do_splat, stdin, stdout):
    result_or_error = {}
    try:
        exec(
            _RUN_CODE_TEMPLATE.format(
                code_string=code_object["codeString"],
                function_name=code_object["functionName"],
            ),
            {
                "_DO_SPLAT": do_splat,
                "_ARGUMENTS": arguments,
                "_RESULT_CACHE": result_or_error,
                "Wikifunctions": wikifunctions.Wikifunctions(stdin, stdout),
                "ZPair": ztypes.ZPair,
                "ZObject": ztypes.ZObject,
            },
            {"exec": None},
        )
    except Exception as error:
        return {"error": _error("Z507", str(error))}
    return result_or_error


# TODO (T282891): Collapse this into function-schemata.
def _error(error_type, message):
    k1_property = error_type + "K1"
    return {
        "Z1K1": {"Z1K1": "Z9", "Z9K1": "Z5"},
        "Z5K1": {"Z1K1": "Z9", "Z9K1": error_type},
        "Z5K2": {
            "Z1K1": {
                "Z1K1": {"Z1K1": "Z9", "Z9K1": "Z7"},
                "Z7K1": {"Z1K1": "Z9", "Z9K1": "Z885"},
                "Z885K1": {"Z1K1": "Z9", "Z9K1": error_type},
            },
            k1_property: {"Z1K1": "Z6", "Z6K1": message},
        },
    }


def _execute_internal(function_call, stdin, stdout):
    # TODO (T282891): Handle input that fails to validate all at once instead of ad hoc.
    function = function_call.get("function", {})
    if function is None:
        return {"error": _error("Z565", "function")}
    if function.get("functionName") is None:
        return {"error": _error("Z565", "functionName")}
    if function.get("codeString") is None:
        return {"error": _error("Z565", "codeString")}

    run_code_object = functools.partial(_run_code_object, stdin=stdin, stdout=stdout)

    # TODO (T289319): Consider whether to reduce all keys to local keys.
    bound_values = {}
    for key, value in function_call.get("functionArguments", {}).items():
        custom_deserializer = value.get("deserializer")
        if custom_deserializer is None:
            deserialized = serialization.deserialize(value["object"])
        else:
            if function_call.get("schemaVersion") == "0.1.1":
                deserializer_code = custom_deserializer.get("code")
                is_list = custom_deserializer.get("isList")
            else:
                deserializer_code = custom_deserializer
                is_list = False
            deserializer_function = lambda x: run_code_object(
                deserializer_code, x, False
            )
            if is_list:
                deserialized = serialization.deserialize(
                    value["object"], next_deserializer=deserializer_function
                )
            else:
                deserialized_dict = deserializer_function(value["object"])
                if deserialized_dict.get("error") is not None:
                    return deserialized_dict
                deserialized = deserialized_dict["result"]
        bound_values[key] = deserialized

    # Run the actual function.
    result_dict = run_code_object(function, bound_values, True)
    if result_dict.get("error") is not None:
        return result_dict
    result = result_dict["result"]

    # Serialize the result.
    custom_serializer = function_call.get("serializer")
    if custom_serializer is None:
        serialized_dict = {"result": serialization.serialize(result)}
    else:
        if function_call.get("schemaVersion") == "0.1.1":
            serializer_code = custom_serializer.get("code")
            is_list = custom_serializer.get("isList")
        else:
            serializer_code = custom_serializer
            is_list = False
        serializer_function = lambda x: run_code_object(serializer_code, x, False)
        if is_list:
            serialized_dict = {
                "result": serialization.serialize(result, serializer_function)
            }
        else:
            serialized_dict = serializer_function(result)
    return serialized_dict


def execute(function_call, stdin=sys.stdin, stdout=sys.stdout):
    try:
        result_or_error = _execute_internal(function_call, stdin, stdout)
    except Exception as e:
        logging.exception(e)
        result_or_error = {"error": _error("Z500", str(e))}
    if result_or_error.get("result") is None and result_or_error.get("error") is None:
        result_or_error["error"] = _error("Z564", "None")
    return [result_or_error.get("result"), result_or_error.get("error")]


def _write_log(message, stdout):
    timestamp = datetime.datetime.now().isoformat()
    stdout.write("start internal log <<<\n")
    stdout.write(message + ", original time: " + timestamp)
    stdout.write("\n")
    stdout.write(">>> end internal log\n")


def main(stdin=sys.stdin, stdout=sys.stdout):
    for line in stdin:
        function_call = json.loads(line)
        if function_call:
            _write_log("calling execute in executor...", stdout)
            result = execute(function_call, stdin, stdout)
            _write_log("...finished calling execute in executor", stdout)
            stdout.write("start result <<<\n")
            stdout.write(json.dumps(result))
            stdout.write("\n")
            stdout.write(">>> end result\n")
            stdout.flush()
            break
    stdout.flush()


if __name__ == "__main__":
    main()

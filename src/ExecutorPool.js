'use strict';

const { getProcessPoolSizeForEnvironment } = require( './environmentUtils.js' );
const { Mutex } = require( '../executors/javascript-wasmedge/function-schemata/javascript/src/utils.js' );

async function killExecutors( hitList ) {
	if ( hitList && hitList.length > 0 ) {
		await Promise.all( hitList.map( async ( executorMap ) => {
			const { executor } = executorMap;
			await executor.immaHeadOut();
		} ) );
	}
	return [];
}

class ExecutorPool {

	constructor( executorInterfaceClass, environment, logger ) {
		this.executorInterfaceClass_ = executorInterfaceClass;
		this.freeExecutorMutex_ = new Mutex();
		this.logger_ = logger;
		// By default, ExecutorPools should be initialized for the current environment.
		this.resetForEnvironment( environment );
	}

	resetForEnvironment( environment ) {
		// No need to await; these processes just need to go away.
		killExecutors( this.freeExecutors_ );
		const processEnv = {};
		for ( const key of Object.keys( environment ) ) {
			processEnv[ key ] = environment[ key ];
		}
		const extraEnvironment = this.executorInterfaceClass_.extraEnvironment;
		for ( const key of Object.keys( extraEnvironment ) ) {
			processEnv[ key ] = extraEnvironment[ key ];
		}
		processEnv.PATH = '/srv/service/programming-languages/wasmedge/wasmedge-binary/bin:' + processEnv.PATH;
		processEnv.LD_LIBRARY_PATH = '/srv/service/programming-languages/wasmedge/wasmedge-binary/lib:' + processEnv.LD_LIBRARY_PATH;
		this.subProcessOptions_ = {
			detached: true,
			shell: true,
			env: processEnv
		};
		const poolSize = getProcessPoolSizeForEnvironment( environment );
		this.freeExecutors_ = [];
		this.fillSync( poolSize );
	}

	fillSync( poolSize ) {
		while ( this.freeExecutors_.length < poolSize ) {
			const hotExecutor = this.executorInterfaceClass_.create( this.logger_ );
			hotExecutor.startExecutorSubprocess( this.subProcessOptions_ );
			this.freeExecutors_.push( {
				executor: hotExecutor,
				promise: Promise.resolve()
			} );
		}
	}

	/*
     * The structure of freeExecutors_:
     *
     *  [
     *      {
     *          executor: <Executor subclass>,
     *          promise: <executor initialization Promise>
     *      }
     *  ]
     *
     * Each Executor contains a Promise which it resolves when it has killed
     * its child process. Subsequent Executors await the resolution of that
     * Promise.
     *
     * We can't avoid having this structure. While it looks like we could simply
     * cause each Promise/async function to resolve to the next Executor,
     * instance, this would mean we wouldn't have access to the previous executor
     * in order to await its processIsDead signal within the next promise.
     * We need IMMEDIATE access to the executor class itself, so it can't be
     * blocked on the promise.
     */
	async getExecutor() {
		const lock = await this.freeExecutorMutex_.acquire();
		const {
			executor: myExecutor,
			promise: myPromise
		} = this.freeExecutors_.shift();
		const nextExecutor = this.executorInterfaceClass_.create();
		const instance = this;
		this.freeExecutors_.push( {
			executor: nextExecutor,
			promise: (
				async () => {
					await myExecutor.processIsDead;
					nextExecutor.startExecutorSubprocess( instance.subProcessOptions_ );
				}
			)()
			// ^ It is VERY IMPORTANT to call this function now. If we don't do
			// this, then all executor processes after the first POOL_SIZE
			// will spin up on demand, rather than having been started early.
		} );
		lock.release();
		await myPromise;
		return myExecutor;
	}

	async drain() {
		const lock = await this.freeExecutorMutex_.acquire();
		const hitList = this.freeExecutors_;
		this.freeExecutors_ = [];
		lock.release();
		await killExecutors( hitList );
	}

}

module.exports = { ExecutorPool };

use axum::extract::Json;
use serde_json::{Value, json};

pub async fn info() -> Json<Value> {
    Json(json!({ "info": "very informational" }))
}

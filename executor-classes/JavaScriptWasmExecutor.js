'use strict';

const softwareLanguages = require( '../executors/javascript-wasmedge/function-schemata/data/definitions/softwareLanguages.json' );

const { Executor } = require( '../src/Executor.js' );
const { ExecutorRegistry } = require( '../src/ExecutorRegistry.js' );
const { programmingLanguageDir } = require( '../src/fileUtils.js' );

const progLangDir_ = programmingLanguageDir();
const quickJSBinary_ = process.env.WASM_BINARY || ( progLangDir_ + '/wasmedge_quickjs.so' );

class JavaScriptWasmExecutor extends Executor {

	static runTheBinary = `${ quickJSBinary_ } /executors/javascript-wasmedge/main.js`;

	static directoryMapping = '--dir .:. --dir /executors/javascript-wasmedge:./executors/javascript-wasmedge';

	static extraEnvironment = {};

	static create( logger ) {
		const executor = new JavaScriptWasmExecutor( logger );
		Object.defineProperty( executor, 'versionString', {
			get: function () {
				// FIXME: Use common config between this and .pipeline/blubber.yaml
				return 'QuickJS v0.5.0-alpha';
			}
		} );
		return executor;
	}

	static register() {
		const codingLanguages = new Set();
		for ( const languageVersion in softwareLanguages ) {
			if ( !( languageVersion.startsWith( 'javascript' ) ) ) {
				continue;
			}
			const languageZID = softwareLanguages[ languageVersion ];
			codingLanguages.add( languageVersion );
			codingLanguages.add( languageZID );
		}
		const executorInterfaceClass = JavaScriptWasmExecutor;
		ExecutorRegistry.registerExecutorConfiguration(
			codingLanguages, executorInterfaceClass );
	}
}

JavaScriptWasmExecutor.register();

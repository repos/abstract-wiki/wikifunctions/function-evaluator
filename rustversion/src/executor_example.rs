extern crate wasmtime;

use std::{
    fs::read_to_string
};

use function_evaluator::executor::{AcceptedErrors, Executor};
use function_evaluator::executor_classes::{
    JavaScriptWasiBuilder, PythonWasiBuilder
};

enum ExecutorType {
    Bad,
    JavaScript,
    Python
}

impl From<&String> for ExecutorType {
    
    fn from(string_version: &String) -> ExecutorType {
        if string_version == "javascript" {
            return ExecutorType::JavaScript;
        }
        if string_version == "python" {
            return ExecutorType::Python;
        }
        ExecutorType::Bad
    }

}

static JAVASCRIPT_INTERPRETER_FILE: &str = "./interpreters/wasip1_quickjs_uncompiled_spliced.wasm";
static PYTHON_INTERPRETER_FILE: &str = "./interpreters/wasip1_rustpython_uncompiled.wasm";

#[tokio::main]
async fn main() -> Result<(), AcceptedErrors> {
    let args = std::env::args().skip(1).collect::<Vec<_>>();

    let file_contents_result = read_to_string(&args[1]);
    assert!(file_contents_result.is_ok());
    let file_contents = file_contents_result.unwrap();
    let deserialized: serde_json::Value = serde_json::from_str(file_contents.as_str()).unwrap();
    let to_run_result = serde_json::to_string(&deserialized);
    assert!(to_run_result.is_ok());
    let to_run = to_run_result.unwrap();

    println!("Initializing Executor ...");
    let executor_type = ExecutorType::from(&args[0]);
    let executor = match executor_type {
        ExecutorType::JavaScript => Ok(Executor::new::<JavaScriptWasiBuilder>(JAVASCRIPT_INTERPRETER_FILE)),
        ExecutorType::Python => Ok(Executor::new::<PythonWasiBuilder>(PYTHON_INTERPRETER_FILE)),
        ExecutorType::Bad => {
            println!("Nope, first arg should be either \"javascript\" or \"python.\"");
            Err(AcceptedErrors::StdError(()))
        }
    };
    println!("Executor is okay!");
    let execution_result = executor.expect("").execute(to_run).await?;

    println!("Stdout contained:\n{}", execution_result);
    Ok(())
}

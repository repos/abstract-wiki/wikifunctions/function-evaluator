use axum::{
    routing::get,
    Router
};

mod endpoints;

pub fn get_app() -> Router {
    let app = Router::new().route("/info", get(endpoints::info));
    app
}

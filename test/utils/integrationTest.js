'use strict';

const assert = require( './assert.js' );

const { deepEqual, isVoid, getError, isZMap, getZMapValue } = require( '../../executors/javascript-wasmedge/function-schemata/javascript/src/utils.js' );
const { convertFormattedRequestToVersionedBinary, convertWrappedZObjectToVersionedBinary } = require( '../../executors/javascript-wasmedge/function-schemata/javascript/src/serialize.js' );
const { Evaluator } = require( '../../src/Evaluator.js' );
const { TestLogger } = require( './TestLogger.js' );

function evaluatorTestInternal(
	name, Z22, expectedOutput = null, expectedErrorType = null, expectedErrorKeyPhrase = '',
	expectedExtraMetadata = [], expectedMissingMetadata = [], actualLogs = [], expectedLogs = []
) {
	const Z22K1 = Z22.Z22K1;
	const Z22K2 = Z22.Z22K2;
	if ( expectedOutput !== null ) {
		assert.deepEqual( Z22K1, expectedOutput.Z22K1, name );
		assert.deepEqual( getError( Z22 ), getError( expectedOutput ), name );
		assert.ok( isZMap( Z22K2 ) );
		checkMetadata( name, Z22K2, expectedExtraMetadata, expectedMissingMetadata );
		checkExpectedLogs( actualLogs, expectedLogs );
	}

	if ( expectedErrorType !== null ) {
		const isError = ( isVoid( Z22K1 ) );
		assert.ok( isError );

		// Check that the error type is correct & the content contains the expected phrase
		const errorType = Z22K2.K1.K1.K2.Z5K1.Z9K1;
		assert.strictEqual( errorType, expectedErrorType );

		const errorMessage = Z22K2.K1.K1.K2.Z5K2[ errorType + 'K1' ].Z6K1;
		assert.include( errorMessage, expectedErrorKeyPhrase );
	}
}

function evaluatorIntegrationTest(
	fetch, testRunner, name, input, expectedOutput = null, expectedErrorType = null, expectedErrorKeyPhrase = '',
	expectedExtraMetadata = [], expectedMissingMetadata = [], useBinaryFormatVersion = undefined
) {
	let binaryBlob;
	let schemaVersion = '0.1.1';
	if ( useBinaryFormatVersion !== undefined ) {
		schemaVersion = useBinaryFormatVersion;
		binaryBlob = convertFormattedRequestToVersionedBinary( input, schemaVersion );
	} else {
		const wrappedInput = {
			reentrant: false,
			zobject: input,
			remainingTime: 15,
			// Request IDs must be distinct.
			requestId: 'thebestrequestid' + name
		};
		binaryBlob = convertWrappedZObjectToVersionedBinary( wrappedInput, schemaVersion );
	}

	testRunner( name, async () => {
		const fetchedResult = await fetch( {
			method: 'POST',
			body: binaryBlob,
			headers: { 'Content-type': 'application/octet-stream' }
		} );
		assert.status( fetchedResult, 200 );
		const jsonRegex = /application\/json/;
		assert.match( fetchedResult.headers.get( 'Content-type' ), jsonRegex );
		const Z22 = await fetchedResult.json();
		evaluatorTestInternal(
			name, Z22, expectedOutput, expectedErrorType, expectedErrorKeyPhrase,
			expectedExtraMetadata, expectedMissingMetadata,
			/* actualLogs= */ [], /* expectedLogs= */ [] );
	} );
}

// This test creates an evaluator for the provided environment and calls
// evaluate() directly, bypassing the service layer. It is useful for testing
// environment variable-based configuration without changing global app
// state.
function executorClassTest(
	environment, testRunner, name, functionCallRequest, expectedOutput = null, expectedErrorType = null, expectedErrorKeyPhrase = '',
	expectedExtraMetadata = [], expectedMissingMetadata = [], expectedLogs = []
) {
	testRunner( name, async () => {
		const logger = new TestLogger();
		const evaluator = Evaluator.create( environment, logger );
		const Z22 = await evaluator.evaluate( functionCallRequest, /* webSocketServer= */ null );
		const actualLogs = logger.logs;
		evaluatorTestInternal(
			name, Z22, expectedOutput, expectedErrorType, expectedErrorKeyPhrase,
			expectedExtraMetadata, expectedMissingMetadata, actualLogs, expectedLogs );
	} );
}

function checkMetadata( name, Z22K2, expectedExtraMetadata = [], expectedMissingMetadata = [] ) {
	// Note: Keep this list in sync with calls to setMetadataValue and setMetadataValues in
	// src/Executor.js::execute(), and in other places (if any).
	// See also: similar code in mswTestRunner.js in the orchestrator.
	// TODO (T382506): Check the values of metadata, especially expectedExtraMetadata.
	const standardMetaData = [
		'evaluationMemoryUsage',
		'evaluationCpuUsage',
		'evaluationStartTime',
		'evaluationEndTime',
		'evaluationDuration',
		'evaluationHostname',
		'executionMemoryUsage',
		'executionCpuUsage',
		'programmingLanguageVersion',
		'wasmedgeGasCost',
		'wasmedgeTotalExecutionTime'
	];

	standardMetaData.forEach( ( key ) => {
		const normalKey = { Z1K1: 'Z6', Z6K1: key };
		const metaDataValue = getZMapValue( Z22K2, normalKey );
		if ( expectedMissingMetadata.includes( key ) ) {
			assert.deepEqual( metaDataValue, undefined, name + ' should not have the `' + key + '` meta-data key set' );
		} else {
			assert.isDefined( metaDataValue, name + ' should have the `' + key + '` meta-data key set' );
		}
	} );

	expectedExtraMetadata.forEach( ( key ) => {
		const normalKey = { Z1K1: 'Z6', Z6K1: key };
		const metaDataValue = getZMapValue( Z22K2, normalKey );
		assert.isDefined( metaDataValue, name + ' should have the `' + key + '` meta-data key set' );
	} );
}

function checkExpectedLogs( actualLogs, expectedLogs ) {
	let actualIndex = 0, absentLog = null;
	while ( expectedLogs.length > 0 ) {
		const nextLog = expectedLogs.shift();
		let wasFound = false;
		while ( actualIndex < actualLogs.length ) {
			const actualLog = actualLogs[ actualIndex ];
			actualIndex += 1;
			if ( deepEqual( actualLog, nextLog ) ) {
				wasFound = true;
				break;
			}
		}
		if ( !wasFound ) {
			absentLog = nextLog;
			break;
		}
	}
	assert.deepEqual( absentLog, null, `All expected logs should have been found; first absent log was ${ absentLog } and actual logs were ${ actualLogs }` );
}

module.exports = {
	executorClassTest,
	evaluatorIntegrationTest
};

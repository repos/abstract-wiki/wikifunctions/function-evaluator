#[cfg(test)]
mod service_tests {

    use axum_test::TestServer;
    use serde_json::json;
    
    use function_evaluator::service::get_app;

    #[tokio::test]
    async fn test_info_route() -> Result<(), anyhow::Error> {
        let app = get_app();
        let server = TestServer::new(app)?;

        let response = server.get("/info").await;

        let expected = json!({
            "info": "very informational"
        });
        assert_eq!(response.status_code(), 200);
        response.assert_json(&expected);
        Ok(())
    }

}

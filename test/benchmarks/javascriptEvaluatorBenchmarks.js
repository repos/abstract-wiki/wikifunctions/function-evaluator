'use strict';

const { javascriptTestData } = require( '../utils/data' );
const { runBenchmarkTests } = require( './benchmarkUtils.js' );

runBenchmarkTests( javascriptTestData );

'use strict';

const fs = require( 'fs' );
const path = require( 'path' );

const softwareLanguages = require( '../executors/javascript-wasmedge/function-schemata/data/definitions/softwareLanguages.json' );

const { Executor } = require( '../src/Executor.js' );
const { ExecutorRegistry } = require( '../src/ExecutorRegistry.js' );
const { programmingLanguageDir } = require( '../src/fileUtils.js' );

const progLangDir_ = programmingLanguageDir();
const rustBinary_ = process.env.WASM_BINARY || ( progLangDir_ + '/rustpython.so' );

let versionString_ = null;

function populateVersionString() {
	const fileName = path.join( 'executors', 'python3-wasmedge', 'python3-all-wasm-version.txt' );
	versionString_ = fs.readFileSync( fileName ).toString().trim();
}

class Python3WasmExecutor extends Executor {

	static runTheBinary = `${ rustBinary_ } -u -m python3.executor`;

	static directoryMapping = '--dir .:./executors';

	static extraEnvironment = { PYTHONPATH: 'executors' };

	static create( logger ) {
		const executor = new Python3WasmExecutor( logger );
		Object.defineProperty( executor, 'versionString', {
			get: function () {
				return versionString_;
			}
		} );
		return executor;
	}

	static register() {
		const codingLanguages = new Set();
		for ( const languageVersion in softwareLanguages ) {
			if ( !( languageVersion.startsWith( 'python' ) ) ) {
				continue;
			}
			const languageZID = softwareLanguages[ languageVersion ];
			codingLanguages.add( languageVersion );
			codingLanguages.add( languageZID );
		}
		const executorInterfaceClass = Python3WasmExecutor;
		ExecutorRegistry.registerExecutorConfiguration(
			codingLanguages, executorInterfaceClass );
	}

}

populateVersionString();
Python3WasmExecutor.register();

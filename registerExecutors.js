'use strict';

/*
 * This file is never used. It is here to satisfy eslint. When we build the
 * language-specific evaluators, we copy the appropriate regster<Thing>.js file
 * to this location, viz. in .pipeline/blubber.yaml:
 *
 *  build-python3-all-wasm-evaluator:
 *    includes: [build-service-layer, add-python3-all-wasm-executor-fresh]
 *    copies:
 *    - from: local
 *      source: ./registerPython3WasmExecutor.js
 *      destination: ./registerExecutors.js
 */

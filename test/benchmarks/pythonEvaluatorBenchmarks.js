'use strict';

const { pythonWasmTestData } = require( '../utils/data' );
const { runBenchmarkTests } = require( './benchmarkUtils.js' );

runBenchmarkTests( pythonWasmTestData );

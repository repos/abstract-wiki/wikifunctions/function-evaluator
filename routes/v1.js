'use strict';

const sUtil = require( '../lib/util' );

/**
 * The main router object
 */
const router = sUtil.router();

// eslint-disable-next-line no-unused-vars
module.exports = ( appObj ) => ( {
	path: '/',
	api_version: 1,
	router
} );

#!/bin/bash
python3 -m pip install -r requirements-format.txt
python3 -m black --check .
python3 -m pyflakes .

'use strict';

const { ServiceUtils } = require( '@wikimedia/service-utils' );
const startApp = require( '../../app.js' );

class TestExpressServer {
	constructor( configDirectory = `${ __dirname }/../../` ) {
		this.configDirectory = configDirectory;
	}

	async start() {
		if ( this.running ) {
			return;
		}
		this.conf = ( await ServiceUtils
			.loadConfig( {
				cwd: this.configDirectory,
				envName: 'test',
				configFile: 'config',
				overrides: { logging: { level: 'debug', format: 'simple' } }
			} ) )
			.services[ 0 ].conf;
		this.app = await startApp( {
			configDirectory: this.configDirectory,
			envName: 'test',
			// Don't start metrics endpoint just in case service is running when tests are running
			startPrometheusServer: false
		} );
		this.uri = `http://${ this.conf.interface || '127.0.0.1' }:${ this.conf.port }/`;
		this.running = true;
	}

	get config() {
		if ( !this.running ) {
			throw new Error( 'Accessing test service config before starting the service' );
		}
		return {
			uri: this.uri,
			conf: this.conf
		};
	}

	async stop() {
		await this.app.close();
	}
}

module.exports = TestExpressServer;

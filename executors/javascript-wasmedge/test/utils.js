import { isString, isObject } from '../function-schemata/javascript/shared/es6UtilsModule.js';

function withoutZ1K1s( ZObject ) {
	if ( !isObject( ZObject ) ) {
		return ZObject;
	}
	const newObject = {};
	for ( const key of Object.keys( ZObject ) ) {
		if ( key === 'Z1K1' ) {
			continue;
		}
		let value = ZObject[ key ];
		if ( !isString( value ) ) {
			value = withoutZ1K1s( value, false );
		}
		newObject[ key ] = value;
	}
	return newObject;
}

export { withoutZ1K1s };

<a href='introduction'></a>
# Wikifunctions function-evaluator

The evaluator service executes user-written 'native' code in a variety of programming languages.
The repository consists of the [evaluator service](#evaluator-service) and a
variety of language-specific [executors](#executors).

## Local installation
You should use one of the Docker images for local use. You do not need to download
the raw code unless you want to modify the evaluator. The Docker images currently
in production are

- [Python WASM](https://docker-registry.wikimedia.org/repos/abstract-wiki/wikifunctions/function-evaluator/wasm-python3-all/tags/)
- [JavaScript WASM](https://docker-registry.wikimedia.org/repos/abstract-wiki/wikifunctions/function-evaluator/wasm-javascript-all/tags/)

If you do plan to modify the code, remember to clone the
repository with the `--recurse-submodules` flag:

```
git clone --recurse-submodules git@gitlab.wikimedia.org:repos/abstract-wiki/wikifunctions/function-evaluator.git
```

If you've already cloned the repository but forgot that flag, you can adjust
your local check-out with:

```
git submodule update --init
```

<a href='building'></a>
We use Blubber as a Docker buildkit in order build our images. Therefore, in order
to build the evaluator locally, ensure that your version of Docker is up-to-date.
You can then build the desired evaluator by running command a like the following
from the `function-evaluator` directory:

```
docker build -f .pipeline/blubber.yaml \
    --target build-python3-all-wasm-evaluator \
    -t build-python3-wasm .
```

It may be helpful to enshrine this as a script somewhere on your $PATH, e.g.

```
#!/bin/bash

set -e

if [ -z $2 ]; then
    echo "Please invoke as $ build-blubber <Blubber variant> <image name>."
    exit 1
fi

docker build -f .pipeline/blubber.yaml --target ${1} -t ${2} .
```

If you have saved this script with the name `build-blubber`, you can invoke it as, e.g.,

```
$ build-blubber build-python3-all-wasm-evaluator build-python3-wasm
```

<a href='testing'></a>
## Testing

### Unit Tests

Unit tests are run in CI, but, if you are developing on the function evaluator,
you will probably want to run them locally. The relevant unit tests are, for the 
evaluator service,

- test-python3-all-wasm-evaluator
- test-javascript-all-wasm-evaluator
- test-python3-all-evaluator (soon to be deprecated)
- test-javascript-all-evaluator (soon to be deprecated)

and, for the executors,

- test-python3-executor
- test-javascript-executor
- test-javascript-wasm-executor (soon to be deprecated)

In order to run the unit tests, build one of the variants listed above (which variants
are relevant will depend on the code you have modified), then `docker run` the
generated image.

Here, too, it can be helpful to automate this process with a script:

```
#!/bin/bash

set -e

if [ -z $2 ]; then
    echo "Please invoke as $ build-run-blubber <Blubber variant> <image name>."
    exit 1
fi

docker build -f .pipeline/blubber.yaml --target ${1} -t ${2} . && \
    docker run ${2}
```

If you save this script on your $PATH in a file named `build-run-blubber`, you can
invoke it as, e.g.,

```
$ build-run-blubber test-python3-all-wasm-evaluator testpywa
```

#### CI Flake

This repo has a couple of flaky tests, 
namely `build-and-test-(python3|javascript)-all-wasm-evaluator`.
We are constantly working to mitigate this flake, but it persists.
If one of these tests fails with a really uninformative error like
`done() was called multiple times` or `write EPIPE`, it's probably just flake.
You can re-run the test in GitLab's UI,
but make sure you also re-run the relevant base image creation job(s)
(`build-wasmedge-quickjs-base` for JavaScript,
`build-wasmedge-rustpython-base` for Python).
This is important because the base images live ephemerally (~1 hour);
if you re-run dependent jobs after the initial pipeline fails,
it's likely that you will be requesting the tests to run
with a now-nonexistent base image.
Therefore, always re-run the base image creation jobs when re-running
other tests.
You can see which tests depend on which base images
by inspecting `.gitlab-ci.yml`.

### Integration Tests

Before submitting, please run the integration tests. Please install Mediawiki core
and the Wikilambda extension.

- Point the extension to a local orchestrator by overriding the
    `$wgWikiLambdaOrchestratorLocation` config value in `LocalSettings.php` with something like:
```
$wgWikiLambdaOrchestratorLocation =  "http://mediawiki-function-orchestrator-1:6254/";
```
- From the MediaWiki root directory, run the command `docker-compose exec mediawiki composer phpunit:entrypoint -- extensions/WikiLambda/tests/phpunit/integration/API/ApiFunctionCallTest.php`
- If your code hasn't made any breaking changes, all tests should pass.

### Ephemeral Production Images

Some image build steps in this software are not fully deterministic and may break
Docker's claim that you can "build once, run anywhere." `wasmedge compile` is the
most frequent offender in this regard.

As a result, you may find that certain things work when images are built and deployed
on the same machine, but not when images have been built and uploaded to docker-registry
for deployment on other machines.

In order to debug this, you can publish untrusted versions of the desired image.
You can do so by adding a new job to `.gitlab-ci.yml`. For example, if you'd like
to build the production Python3 evaluator, you can add a job like

```
publish-python3-all-wasm-untrusted:
  extends: .kokkuri:build-and-publish-image
  stage: publish-untrusted-image
  variables:
    BUILD_VARIANT: production-python3-all-wasm
    PUBLISH_IMAGE_EXTRA_TAGS: latest
    PUBLISH_IMAGE_TAG: $KOKKURI_PIPELINE_TIMESTAMP
    # eslint-disable-next-line max-len
    PUBLISH_IMAGE_NAME: repos/abstract-wiki/wikifunctions/function-evaluator/wasm-python3-all
```

You should then add `publish-untrusted-image` (note the stage name above) as a
stage to the list of `stages` in `gitlab-ci.yml` (if you make it the first stage,
it will run first). If you open an MR in GitLab, you can then provoke the CI pipeline
to run this job. An image built using the specified Blubber variant (in this case,
`production-python3-all-wasm`) will be uploaded to `registry.cloud.releng.team/repos/etc`.
There are no guarantees on persistence, so pull this images as soon as possible if
you intend to use it, with a command like

```
docker pull registry.cloud.releng.team/repos/abstract-wiki/wikifunctions/function-evaluator/wasm-python3-all:latest
```

<a href='evaluator-service'></a>
## Evaluator Service
The evaluator itself is a thin wrapper (based on [service-template-node](https://www.mediawiki.org/wiki/ServiceTemplateNode))
responsible for validating incoming objects as Z7/Function calls, determining the correct
[executor](#executors) to delegate to, and returning either the result of
function execution or an appropriate error back to the caller.

## wasmedge Sandboxing
Executors are run inside of a WASM sandbox. The WASM runtime we use is `wasmedge`.

After execution, the `wasmedge` command spits out some statistics (due to the 
`--enable-all-statistics` flag). The tracked statistics are

- Total execution time: X ns
- Wasm instructions execution time: X ns
- Host functions execution time: X ns
- Executed wasm instructions count: X
- Gas costs: X
- Instructions per second: X

`Total execution time` and `Gas costs` are returned as metadata.

<a href='executors'></a>
## Executors
An executor accepts as input a fully dereferenced Z7/Function call (with a single native-code implementation),
executes the native code with the provided inputs, and returns a Z22/Pair containing
in key Z22K1 either the result of successful execution or Z24/Void, and in Z22K2 any appropriate Z5/Errors.

### Communication between the Evaluator and Executor
As currently implemented, executors run as child processes of the main Node
process. The evaluator initializes a child process corresponding to the correct
programming language and runs the appropriate executor module. For example,
in order to execute Python code, the evaluator spawns a subprocess with the
command `python3 executors/python3/executor.py`.

Thereafter, the evaluator communicates with the executor via I/O streams:
`stdin`, `stdout`, and `stderr`. The evaluator writes the (JSON-stringified)
Z7/Function call to the child process's `stdin`. The evaluator then waits until the
subprocess terminates. Logs written to `stderr` in the executor will likewise
be logged by the evaluator. The executor writes the (JSON-stringified) Z22/Pair
to `stdout`, which the evaluator collects and returns as JSON.

### Implementing a New Executor
An executor must be able to do three things: 1) communicate with the main
process via standard I/O streams; 2) deserialize ZObjects as appropriate native
types (and perform the inverse serialization operation); and 3) execute code
(via constructions like, e.g., `exec` or `eval`).
The existing Python and JavaScript implementations can hopefully serve as a
reference for how to accomplish these tasks, but serialization and deserialization
deserve particular attention.

For certain built-in types, there is a natural mapping between
[ZType](https://meta.wikimedia.org/wiki/Abstract_Wikipedia/Reserved_ZIDs#Core_types)
and primitive (or standard library) types; currently, executors can
(de)serialize the following types:

- Z6/String          <-> string
- Z21/Unit           <-> nullary type (None, null, etc.)
- Z40/Boolean        <-> Boolean
- Z881/Typed list    <-> sequential container type (list, array, vector, etc.)
- Z882/Typed pair    <-> pair container type (tuple, etc.)
- Z883/Typed map     <-> key-value container type (map, dict, etc.)

Any new executor must be able to support these. We expect the list of built-in
types to grow (a little, and finitely); we also expect to be able to support
(de)serialization of user-defined types, but executors can't handle this at
present.

Serialization is completely determined by introspection of types in each programming
language. A str or String serializes as a Z6/String; a list, tuple, Array, etc. as a
Z7 call to a Z881/Typed list; etc.

To be precise: deserialization can trivially determine the type of a Z1/ZObject by
consulting its `Z1K1` attribute, which reports the ZID corresponding to its type.
However, serialization must rely on the language's introspection capabilities
or some other strategy to determine type.

<a href='repository-stewardship'></a>
## Formatting Python Source Code

- ### Create a Python Virtual Environment
```
VENVNAME=venv
python3.6 -m venv ${VENVNAME}
# Any Python version >= 3.6 will work; 3.6 is chosen here to keep pace with the
# base Docker image in .pipeline/blubber.yaml#build-format-python3
. ./${VENVNAME}/bin/activate
pip install -r executors/python3/requirements-format.txt
```

- ### Run Black
```
python -m black executors/python3
```

<a href='webassembly'></a>
# WebAssembly (WASM) and WebAssembly System Interface (WASI)

Because WASI is still something of an emerging standard,
not every tool implements the same specification.
Version differences in WASI and associated APIs complicate this situation still further.

Currently, the JS and Python evaluator builders
use different Rust targets when building the executor binaries.
JS uses `wasm32-wasip1`, while Python uses `wasm32-wasi`.
This is because of a breaking change made on January 9, 2025,
described in [this Rust blog post](https://blog.rust-lang.org/2024/04/09/updates-to-rusts-wasi-targets.html).
`wasm32-wasip1` is advertised as simply a renamed `wasm32-wasi`,
but this is apparently not the case.
The result of this has been that, at very least,
the `deflateInit2_` WASI syscall does not work as expected in RustPython.
The issue here may be with RustPython
or with `wasmedge`, the WASI runtime currently in use in the evaluator.

Both of these dependencies should be revisited.
`wasmedge` isn't necessarily the best or most compliant
WASI runtime out there.
Likewise, RustPython isn't what people normally think of
when they think of Python.
Fortunately, the CPython reference implementation for Python
[can now be compiled to WASM](https://github.com/python/cpython/blob/main/Tools/wasm/README.md),
making it possible for us to consider switching Python implementations.

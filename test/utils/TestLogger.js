'use strict';

class TestLogger {

	constructor() {
		this.logs_ = [];
		Object.defineProperty( this, 'logs', {
			get: function () {
				return Object.freeze( this.logs_ );
			}
		} );
	}

	log( ...args ) {
		this.logs_.push( args );
	}

}

module.exports = { TestLogger };

import argparse
import pathlib
import yaml


def main(
    input_blubber_file, variant, add_base_image, remove_includes, output_blubber_file
):
    if add_base_image is None and remove_includes is None:
        raise Exception("Must supply either --add-base-image or --remove-includes.")
    # PyYaml doesn't respect comments, so we have to read the docker buildkit
    # syntax line separately, then write it out explicitly.
    with open(input_blubber_file, "r") as inp:
        syntax_line = inp.readline().strip()
        blubber_dict = yaml.full_load(inp)

    the_variant = blubber_dict.get("variants", {}).get(variant)
    if the_variant is None:
        raise Exception(f"Variant {variant} not found in input Blubber file.")

    if add_base_image is not None:
        the_variant["base"] = add_base_image

    if remove_includes is not None:
        original_includes = the_variant.get("includes", [])
        to_remove = set(remove_includes.split(","))
        new_includes = [
            include for include in original_includes if include not in to_remove
        ]
        the_variant["includes"] = new_includes
    with open(output_blubber_file, "w") as outp:
        # PyYaml doesn't respect comments, so we have to read the docker buildkit
        # syntax line separately, then write it out explicitly.
        outp.write(f"{syntax_line}\n")
        outp.write(yaml.dump(blubber_dict))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input-blubber-file",
        type=pathlib.Path,
        required=True,
        help="Original Blubber file",
    )
    parser.add_argument(
        "--output-blubber-file",
        type=pathlib.Path,
        required=True,
        help="Generated Blubber file",
    )
    parser.add_argument(
        "--variant", type=str, required=True, help="Blubber variant to modify"
    )
    parser.add_argument(
        "--add-base-image", type=str, help="Base image to add to variant"
    )
    parser.add_argument(
        "--remove-includes", type=str, help="Variant includes to remove from variant"
    )
    args = parser.parse_args()
    main(
        args.input_blubber_file,
        args.variant,
        args.add_base_image,
        args.remove_includes,
        args.output_blubber_file,
    )

"""Module that handles reentrant websocket calls to orchestrator.

This needs a lot of work. stdin needs to be captured somewhere in order to
route different results to the appropriate calls. Threaded approach with
a central handler for stdin could help. Probably also need asyncio here.
"""

import json
from . import serialization


_DEBUG_ZID = "Z820"


class Wikifunctions:
    def __init__(self, stdin, stdout):
        self._stdin = stdin
        self._stdout = stdout

    def Call(self, function_zid, *args, **kwargs):
        Z7 = {"Z1K1": "Z7", "Z7K1": function_zid}
        for key, value in kwargs.items():
            Z7[key] = serialization.serialize(value)
        self._stdout.write("start reentrant call <<<\n")
        self._stdout.write(json.dumps(Z7) + "\n")
        self._stdout.write(">>> end reentrant call\n")
        # flush() is crucial to ensure that the call be made promptly.
        self._stdout.flush()
        while True:
            for line in self._stdin:
                if not line.strip():
                    continue
                return serialization.deserialize(json.loads(line))

    def Debug(self, logged_string):
        self.Z820("executorDebugLogs", logged_string)

    def Z820(self, key, log_string):
        payload = {"key": key, "logString": log_string}
        self._stdout.write("start debug log <<<\n")
        self._stdout.write(json.dumps(payload) + "\n")
        self._stdout.write(">>> end debug log\n")
        self._stdout.flush()

    def __call__(self, function_zid):
        if function_zid == _DEBUG_ZID:
            return self.Z820

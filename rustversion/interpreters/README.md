# Interpreters

These are included here for convenience.
In production, we will continue to build the WASM interpreters from source.

For now, here are instructions on how to recreate the interpreters in this directory.

## Helper scripts

Run this from `function-evaluator` root:

```
# build-blubber
docker build -f .pipeline/blubber.yaml --target ${1} -t ${2} .
```

## Python Interpreter

```
build-blubber test-python3-all-wasm-evaluator testpy
docker cp $(docker create testpy):/srv/service/programming-languages/rustpython_uncompiled.wasm ./rustversion/interpreters
```

## JS Interpreter

1. In `function-evaluator/executors/wasm-utilities/install-wasmedge-quickjs`,
ensure that the quickjs version is pinned to `0.6.1-alpha` if it is not
already.

```
git checkout 0.6.1-alpha
```

2. Build the JS test image and pull the uncompiled wasm binary out of the image.

```
build-blubber test-javascript-all-wasm-evaluator testjs
docker cp $(docker create testjs):/srv/service/programming-languages/wasmedge_quickjs_uncompiled.wasm .
```

3. Build the [wabt](https://github.com/WebAssembly/wabt) tools from source.

4. Convert `wasmedge_quickjs_uncompiled.wasm` to `.wat`:

```
wabt/build/wasm2wat wasmedge_quickjs_uncompiled.wasm > quickjs.wat
```

5. Apply `quickjs.diff`:

```
patch quickjs.wat < interpreters/quickjs.diff
```

We need this diff because the QuickJS .wasm file implements `sock_accept`
with two parameters (correctly) while the `wasmtime` implementation in
Rust expects it to have three (which seems incorrect).
This diff bypasses the `sock_accept` issue by making it comply with wasmtime's
API contract, declaring three arguments and providing three arguments at the
call site. The semantics of the call are certainly wrong, but `sock_accept`
should never be called inside of our WASM executors anyway.

6. Turn `.wat` back into `.wasm`:

```
wabt/build/wat2wasm quickjs.wat -o rustversion/interpreters/spliced_quickjs_uncompiled.wasm
```

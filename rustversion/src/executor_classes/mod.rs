use ambient_authority::ambient_authority;
use wasi_common::{
    sync::Dir,
    tokio::WasiCtxBuilder
};

// TODO (T380808): Find a more idiomatic pattern for this. The idea here is to be able
// to surface a number of error types with ?. Here, we're just wrapping them
// in an enum and losing the original error.
// Also, merge this with AcceptedErrors in src/executor.rs.
// Also, use the thiserror create and error! macro to facilitate error functionality.
#[derive(Debug)]
pub enum ExecutorErrors {
    StdError(()),
    StringArrayError(()),
    WasiError(()),
}

impl From<wasi_common::StringArrayError> for ExecutorErrors {
    fn from(_error: wasi_common::StringArrayError) -> ExecutorErrors {
        ExecutorErrors::StringArrayError(())
    }
}

impl From<wasi_common::Error> for ExecutorErrors {
    fn from(_error: wasi_common::Error) -> ExecutorErrors {
        ExecutorErrors::WasiError(())
    }
}

impl From<std::io::Error> for ExecutorErrors {
    fn from(_error: std::io::Error) -> ExecutorErrors {
        ExecutorErrors::StdError(())
    }
}

pub trait ExecutorWasiBuilder {
    fn build_for_executor(builder: &mut WasiCtxBuilder, wasm_file: &str) -> Result<(), ExecutorErrors>;
}

pub struct PythonWasiBuilder {}

impl ExecutorWasiBuilder for PythonWasiBuilder {

    fn build_for_executor(builder: &mut WasiCtxBuilder, wasm_file: &str) -> Result<(), ExecutorErrors> {
        let the_dir = Dir::open_ambient_dir(
            "../executors",
            ambient_authority())?;
        builder
            .args(&[
                String::from(wasm_file),
                String::from("-u"),
                String::from("-m"),
                String::from("python3.executor")])?
            .preopened_dir(the_dir, ".")?
            .inherit_env()?;
        Ok(())
    }

}

pub struct JavaScriptWasiBuilder {}

impl ExecutorWasiBuilder for JavaScriptWasiBuilder {

    fn build_for_executor(builder: &mut WasiCtxBuilder, wasm_file: &str) -> Result<(), ExecutorErrors> {
        let javascript_wasmedge_dir = Dir::open_ambient_dir(
            "../executors/javascript-wasmedge",
            ambient_authority())?;
        builder
            .args(&[
                String::from(wasm_file),
                String::from("./main.js")])?
            .preopened_dir(javascript_wasmedge_dir, ".")?
            .inherit_env()?;
        Ok(())
    }

}
